// by Christopher Graves

import {BuilderHUD} from './modules/src_modules_BuilderHUD'
import {spawnGltfX} from './modules/src_modules_SpawnerFunctions'
const scene = new Entity()
const transform = new Transform({
  position: new Vector3(0, 0, 0),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})


///////// Example of adding the BuilderHUD to your scene and attaching it to one existing scene entity.
const hud:BuilderHUD =  new BuilderHUD()



scene.addComponentOrReplace(transform)
engine.addEntity(scene)


// spawnGLTFX modules blender shack model
let shackShape = new GLTFShape('models/shack.glb')
let shack = spawnGltfX(shackShape, 20,.3,80,	0,0,0,		1,1,1) 
engine.addEntity(shack)
hud.attachToEntity(shack)

const treeFir_01 = new Entity()
treeFir_01.setParent(scene)
const gltfShape = new GLTFShape('models/TreeFir_01/TreeFir_01.glb')
treeFir_01.addComponentOrReplace(gltfShape)
const transform_2 = new Transform({
  position: new Vector3(6, 0, 3),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
treeFir_01.addComponentOrReplace(transform_2)
engine.addEntity(treeFir_01)

const treeFir_01_2 = new Entity()
treeFir_01_2.setParent(scene)
treeFir_01_2.addComponentOrReplace(gltfShape)
const transform_3 = new Transform({
  position: new Vector3(3, 0, 3),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
treeFir_01_2.addComponentOrReplace(transform_3)
engine.addEntity(treeFir_01_2)

const treeFir_01_3 = new Entity()
treeFir_01_3.setParent(scene)
treeFir_01_3.addComponentOrReplace(gltfShape)
const transform_4 = new Transform({
  position: new Vector3(1.5, 0, 7),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
treeFir_01_3.addComponentOrReplace(transform_4)
engine.addEntity(treeFir_01_3)

const treeFir_01_4 = new Entity()
treeFir_01_4.setParent(scene)
treeFir_01_4.addComponentOrReplace(gltfShape)
const transform_5 = new Transform({
  position: new Vector3(1.5, 0, 16),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
treeFir_01_4.addComponentOrReplace(transform_5)
engine.addEntity(treeFir_01_4)

const floorBlock_04 = new Entity()
floorBlock_04.setParent(scene)
const gltfShape_2 = new GLTFShape('models/FloorBlock_04/FloorBlock_04.glb')
floorBlock_04.addComponentOrReplace(gltfShape_2)
const transform_6 = new Transform({
  position: new Vector3(33, 0, 27),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865475),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04.addComponentOrReplace(transform_6)
engine.addEntity(floorBlock_04)

const floorBlock_04_2 = new Entity()
floorBlock_04_2.setParent(scene)
floorBlock_04_2.addComponentOrReplace(gltfShape_2)
const transform_7 = new Transform({
  position: new Vector3(6.5, 0, 17.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_2.addComponentOrReplace(transform_7)
engine.addEntity(floorBlock_04_2)

const chair_01 = new Entity()
chair_01.setParent(scene)
const gltfShape_3 = new GLTFShape('models/Chair_01/Chair_01.glb')
chair_01.addComponentOrReplace(gltfShape_3)
const transform_8 = new Transform({
  position: new Vector3(4.5, 0, 21),
  rotation: new Quaternion(0, -0.9238795325112872, 0, 0.38268343236509006),
  scale: new Vector3(1, 1, 1)
})
chair_01.addComponentOrReplace(transform_8)
engine.addEntity(chair_01)

const chair_01_2 = new Entity()
chair_01_2.setParent(scene)
chair_01_2.addComponentOrReplace(gltfShape_3)
const transform_9 = new Transform({
  position: new Vector3(12.5, 0, 9.5),
  rotation: new Quaternion(0, 0.38268343236508984, 0, 0.9238795325112871),
  scale: new Vector3(1, 1, 1)
})
chair_01_2.addComponentOrReplace(transform_9)
engine.addEntity(chair_01_2)

const floorBlock_04_3 = new Entity()
floorBlock_04_3.setParent(scene)
floorBlock_04_3.addComponentOrReplace(gltfShape_2)
const transform_10 = new Transform({
  position: new Vector3(4.5, 0, 19),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_3.addComponentOrReplace(transform_10)
engine.addEntity(floorBlock_04_3)

const floorBlock_04_4 = new Entity()
floorBlock_04_4.setParent(scene)
floorBlock_04_4.addComponentOrReplace(gltfShape_2)
const transform_11 = new Transform({
  position: new Vector3(15, 0, 21.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_4.addComponentOrReplace(transform_11)
engine.addEntity(floorBlock_04_4)

const floorBlock_04_5 = new Entity()
floorBlock_04_5.setParent(scene)
floorBlock_04_5.addComponentOrReplace(gltfShape_2)
const transform_12 = new Transform({
  position: new Vector3(13, 0, 21.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_5.addComponentOrReplace(transform_12)
engine.addEntity(floorBlock_04_5)

const floorBlock_04_6 = new Entity()
floorBlock_04_6.setParent(scene)
floorBlock_04_6.addComponentOrReplace(gltfShape_2)
const transform_13 = new Transform({
  position: new Vector3(11, 0, 21.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_6.addComponentOrReplace(transform_13)
engine.addEntity(floorBlock_04_6)

const floorBlock_04_7 = new Entity()
floorBlock_04_7.setParent(scene)
floorBlock_04_7.addComponentOrReplace(gltfShape_2)
const transform_14 = new Transform({
  position: new Vector3(9, 0, 21.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_7.addComponentOrReplace(transform_14)
engine.addEntity(floorBlock_04_7)

const floorBlock_04_8 = new Entity()
floorBlock_04_8.setParent(scene)
floorBlock_04_8.addComponentOrReplace(gltfShape_2)
const transform_15 = new Transform({
  position: new Vector3(8.5, 0, 20),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_8.addComponentOrReplace(transform_15)
engine.addEntity(floorBlock_04_8)

const floorBlock_04_9 = new Entity()
floorBlock_04_9.setParent(scene)
floorBlock_04_9.addComponentOrReplace(gltfShape_2)
const transform_16 = new Transform({
  position: new Vector3(6.5, 0, 21),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_9.addComponentOrReplace(transform_16)
engine.addEntity(floorBlock_04_9)

const floorBlock_04_10 = new Entity()
floorBlock_04_10.setParent(scene)
floorBlock_04_10.addComponentOrReplace(gltfShape_2)
const transform_17 = new Transform({
  position: new Vector3(7, 0, 21.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_10.addComponentOrReplace(transform_17)
engine.addEntity(floorBlock_04_10)

const floorBlock_04_11 = new Entity()
floorBlock_04_11.setParent(scene)
floorBlock_04_11.addComponentOrReplace(gltfShape_2)
const transform_18 = new Transform({
  position: new Vector3(9, 0, 9),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_11.addComponentOrReplace(transform_18)
engine.addEntity(floorBlock_04_11)

const floorBlock_04_12 = new Entity()
floorBlock_04_12.setParent(scene)
floorBlock_04_12.addComponentOrReplace(gltfShape_2)
const transform_19 = new Transform({
  position: new Vector3(11, 0, 9),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_12.addComponentOrReplace(transform_19)
engine.addEntity(floorBlock_04_12)

const floorBlock_04_13 = new Entity()
floorBlock_04_13.setParent(scene)
floorBlock_04_13.addComponentOrReplace(gltfShape_2)
const transform_20 = new Transform({
  position: new Vector3(5, 0, 21.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_13.addComponentOrReplace(transform_20)
engine.addEntity(floorBlock_04_13)

const floorBlock_04_14 = new Entity()
floorBlock_04_14.setParent(scene)
floorBlock_04_14.addComponentOrReplace(gltfShape_2)
const transform_21 = new Transform({
  position: new Vector3(8.5, 0, 21.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_14.addComponentOrReplace(transform_21)
engine.addEntity(floorBlock_04_14)

const floorBlock_04_15 = new Entity()
floorBlock_04_15.setParent(scene)
floorBlock_04_15.addComponentOrReplace(gltfShape_2)
const transform_22 = new Transform({
  position: new Vector3(10.5, 0, 12.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_15.addComponentOrReplace(transform_22)
engine.addEntity(floorBlock_04_15)

const floorBlock_04_16 = new Entity()
floorBlock_04_16.setParent(scene)
floorBlock_04_16.addComponentOrReplace(gltfShape_2)
const transform_23 = new Transform({
  position: new Vector3(12.5, 0, 11),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_16.addComponentOrReplace(transform_23)
engine.addEntity(floorBlock_04_16)

const floorBlock_04_17 = new Entity()
floorBlock_04_17.setParent(scene)
floorBlock_04_17.addComponentOrReplace(gltfShape_2)
const transform_24 = new Transform({
  position: new Vector3(10.5, 0, 11),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_17.addComponentOrReplace(transform_24)
engine.addEntity(floorBlock_04_17)

const floorBlock_04_18 = new Entity()
floorBlock_04_18.setParent(scene)
floorBlock_04_18.addComponentOrReplace(gltfShape_2)
const transform_25 = new Transform({
  position: new Vector3(4.5, 0, 21),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_18.addComponentOrReplace(transform_25)
engine.addEntity(floorBlock_04_18)

const floorBlock_04_19 = new Entity()
floorBlock_04_19.setParent(scene)
floorBlock_04_19.addComponentOrReplace(gltfShape_2)
const transform_26 = new Transform({
  position: new Vector3(8.5, 0, 11),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_19.addComponentOrReplace(transform_26)
engine.addEntity(floorBlock_04_19)

const floorBlock_04_20 = new Entity()
floorBlock_04_20.setParent(scene)
floorBlock_04_20.addComponentOrReplace(gltfShape_2)
const transform_27 = new Transform({
  position: new Vector3(8.5, 0, 13),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_20.addComponentOrReplace(transform_27)
engine.addEntity(floorBlock_04_20)

const floorBlock_04_21 = new Entity()
floorBlock_04_21.setParent(scene)
floorBlock_04_21.addComponentOrReplace(gltfShape_2)
const transform_28 = new Transform({
  position: new Vector3(6.5, 0, 19),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_21.addComponentOrReplace(transform_28)
engine.addEntity(floorBlock_04_21)

const floorBlock_04_22 = new Entity()
floorBlock_04_22.setParent(scene)
floorBlock_04_22.addComponentOrReplace(gltfShape_2)
const transform_29 = new Transform({
  position: new Vector3(6.5, 0, 13.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_22.addComponentOrReplace(transform_29)
engine.addEntity(floorBlock_04_22)

const floorBlock_04_23 = new Entity()
floorBlock_04_23.setParent(scene)
floorBlock_04_23.addComponentOrReplace(gltfShape_2)
const transform_30 = new Transform({
  position: new Vector3(8.5, 0, 19),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_23.addComponentOrReplace(transform_30)
engine.addEntity(floorBlock_04_23)

const floorBlock_04_24 = new Entity()
floorBlock_04_24.setParent(scene)
floorBlock_04_24.addComponentOrReplace(gltfShape_2)
const transform_31 = new Transform({
  position: new Vector3(8.5, 0, 17),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_24.addComponentOrReplace(transform_31)
engine.addEntity(floorBlock_04_24)

const floorBlock_04_25 = new Entity()
floorBlock_04_25.setParent(scene)
floorBlock_04_25.addComponentOrReplace(gltfShape_2)
const transform_32 = new Transform({
  position: new Vector3(10, 0, 14.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_25.addComponentOrReplace(transform_32)
engine.addEntity(floorBlock_04_25)

const floorBlock_04_26 = new Entity()
floorBlock_04_26.setParent(scene)
floorBlock_04_26.addComponentOrReplace(gltfShape_2)
const transform_33 = new Transform({
  position: new Vector3(12.5, 0, 12.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_26.addComponentOrReplace(transform_33)
engine.addEntity(floorBlock_04_26)

const floorBlock_04_27 = new Entity()
floorBlock_04_27.setParent(scene)
floorBlock_04_27.addComponentOrReplace(gltfShape_2)
const transform_34 = new Transform({
  position: new Vector3(10.5, 0, 20),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_27.addComponentOrReplace(transform_34)
engine.addEntity(floorBlock_04_27)

const floorBlock_04_28 = new Entity()
floorBlock_04_28.setParent(scene)
floorBlock_04_28.addComponentOrReplace(gltfShape_2)
const transform_35 = new Transform({
  position: new Vector3(10, 0, 18.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_28.addComponentOrReplace(transform_35)
engine.addEntity(floorBlock_04_28)

const floorBlock_04_29 = new Entity()
floorBlock_04_29.setParent(scene)
floorBlock_04_29.addComponentOrReplace(gltfShape_2)
const transform_36 = new Transform({
  position: new Vector3(4.5, 0, 13.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_29.addComponentOrReplace(transform_36)
engine.addEntity(floorBlock_04_29)

const floorBlock_04_30 = new Entity()
floorBlock_04_30.setParent(scene)
floorBlock_04_30.addComponentOrReplace(gltfShape_2)
const transform_37 = new Transform({
  position: new Vector3(4.5, 0, 11.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_30.addComponentOrReplace(transform_37)
engine.addEntity(floorBlock_04_30)

const floorBlock_04_31 = new Entity()
floorBlock_04_31.setParent(scene)
floorBlock_04_31.addComponentOrReplace(gltfShape_2)
const transform_38 = new Transform({
  position: new Vector3(4.5, 0, 15.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_31.addComponentOrReplace(transform_38)
engine.addEntity(floorBlock_04_31)

const floorBlock_04_32 = new Entity()
floorBlock_04_32.setParent(scene)
floorBlock_04_32.addComponentOrReplace(gltfShape_2)
const transform_39 = new Transform({
  position: new Vector3(12, 0, 14.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_32.addComponentOrReplace(transform_39)
engine.addEntity(floorBlock_04_32)

const floorBlock_04_33 = new Entity()
floorBlock_04_33.setParent(scene)
floorBlock_04_33.addComponentOrReplace(gltfShape_2)
const transform_40 = new Transform({
  position: new Vector3(12, 0, 16.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_33.addComponentOrReplace(transform_40)
engine.addEntity(floorBlock_04_33)

const floorBlock_04_34 = new Entity()
floorBlock_04_34.setParent(scene)
floorBlock_04_34.addComponentOrReplace(gltfShape_2)
const transform_41 = new Transform({
  position: new Vector3(6.5, 0, 11.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_34.addComponentOrReplace(transform_41)
engine.addEntity(floorBlock_04_34)

const floorBlock_04_35 = new Entity()
floorBlock_04_35.setParent(scene)
floorBlock_04_35.addComponentOrReplace(gltfShape_2)
const transform_42 = new Transform({
  position: new Vector3(6.5, 0, 15.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_35.addComponentOrReplace(transform_42)
engine.addEntity(floorBlock_04_35)

const floorBlock_04_36 = new Entity()
floorBlock_04_36.setParent(scene)
floorBlock_04_36.addComponentOrReplace(gltfShape_2)
const transform_43 = new Transform({
  position: new Vector3(4.5, 0, 17),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_36.addComponentOrReplace(transform_43)
engine.addEntity(floorBlock_04_36)

const chinesePergola_01 = new Entity()
chinesePergola_01.setParent(scene)
const gltfShape_4 = new GLTFShape('models/ChinesePergola_01/ChinesePergola_01.glb')
chinesePergola_01.addComponentOrReplace(gltfShape_4)
const transform_44 = new Transform({
  position: new Vector3(5.5, 0, 8),
  rotation: new Quaternion(0, 0.471396736825998, 0, 0.8819212643483552),
  scale: new Vector3(1, 1, 1)
})
chinesePergola_01.addComponentOrReplace(transform_44)
engine.addEntity(chinesePergola_01)

const chineseGate_01 = new Entity()
chineseGate_01.setParent(scene)
const gltfShape_5 = new GLTFShape('models/ChineseGate_01/ChineseGate_01.glb')
chineseGate_01.addComponentOrReplace(gltfShape_5)
const transform_45 = new Transform({
  position: new Vector3(37.5, 0, 27),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865475),
  scale: new Vector3(1, 1, 1)
})
chineseGate_01.addComponentOrReplace(transform_45)
engine.addEntity(chineseGate_01)

const floorBlock_04_37 = new Entity()
floorBlock_04_37.setParent(scene)
floorBlock_04_37.addComponentOrReplace(gltfShape_2)
const transform_46 = new Transform({
  position: new Vector3(12.5, 0, 9),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_37.addComponentOrReplace(transform_46)
engine.addEntity(floorBlock_04_37)

const treeFir_01_5 = new Entity()
treeFir_01_5.setParent(scene)
treeFir_01_5.addComponentOrReplace(gltfShape)
const transform_47 = new Transform({
  position: new Vector3(1.5, 0, 10.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
treeFir_01_5.addComponentOrReplace(transform_47)
engine.addEntity(treeFir_01_5)

const treeFir_01_6 = new Entity()
treeFir_01_6.setParent(scene)
treeFir_01_6.addComponentOrReplace(gltfShape)
const transform_48 = new Transform({
  position: new Vector3(11.5, 0, 24),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
treeFir_01_6.addComponentOrReplace(transform_48)
engine.addEntity(treeFir_01_6)

const treeFir_01_7 = new Entity()
treeFir_01_7.setParent(scene)
treeFir_01_7.addComponentOrReplace(gltfShape)
const transform_49 = new Transform({
  position: new Vector3(10.5, 0, 3.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
treeFir_01_7.addComponentOrReplace(transform_49)
engine.addEntity(treeFir_01_7)

const floorBlock_04_38 = new Entity()
floorBlock_04_38.setParent(scene)
floorBlock_04_38.addComponentOrReplace(gltfShape_2)
const transform_50 = new Transform({
  position: new Vector3(29, 0, 27),
  rotation: new Quaternion(0.004755127342809566, 0.7028152458818533, 0.004812806079315271, 0.7113402531964754),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_38.addComponentOrReplace(transform_50)
engine.addEntity(floorBlock_04_38)

const floorBlock_04_39 = new Entity()
floorBlock_04_39.setParent(scene)
floorBlock_04_39.addComponentOrReplace(gltfShape_2)
const transform_51 = new Transform({
  position: new Vector3(27, 0, 27),
  rotation: new Quaternion(0.004755127342809566, 0.7028152458818533, 0.004812806079315271, 0.7113402531964754),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_39.addComponentOrReplace(transform_51)
engine.addEntity(floorBlock_04_39)

const floorBlock_04_40 = new Entity()
floorBlock_04_40.setParent(scene)
floorBlock_04_40.addComponentOrReplace(gltfShape_2)
const transform_52 = new Transform({
  position: new Vector3(25, 0, 27),
  rotation: new Quaternion(0.004755127342809566, 0.7028152458818533, 0.004812806079315271, 0.7113402531964754),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_40.addComponentOrReplace(transform_52)
engine.addEntity(floorBlock_04_40)

const floorBlock_04_41 = new Entity()
floorBlock_04_41.setParent(scene)
floorBlock_04_41.addComponentOrReplace(gltfShape_2)
const transform_53 = new Transform({
  position: new Vector3(19, 0, 27),
  rotation: new Quaternion(0.004755127342809566, 0.7028152458818533, 0.004812806079315271, 0.7113402531964754),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_41.addComponentOrReplace(transform_53)
engine.addEntity(floorBlock_04_41)

const floorBlock_04_42 = new Entity()
floorBlock_04_42.setParent(scene)
floorBlock_04_42.addComponentOrReplace(gltfShape_2)
const transform_54 = new Transform({
  position: new Vector3(31, 0, 27),
  rotation: new Quaternion(0.004755127342809566, 0.7028152458818533, 0.004812806079315271, 0.7113402531964754),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_42.addComponentOrReplace(transform_54)
engine.addEntity(floorBlock_04_42)

const floorBlock_04_43 = new Entity()
floorBlock_04_43.setParent(scene)
floorBlock_04_43.addComponentOrReplace(gltfShape_2)
const transform_55 = new Transform({
  position: new Vector3(23, 0, 27),
  rotation: new Quaternion(0.004755127342809566, 0.7028152458818533, 0.004812806079315271, 0.7113402531964754),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_43.addComponentOrReplace(transform_55)
engine.addEntity(floorBlock_04_43)

const floorBlock_04_44 = new Entity()
floorBlock_04_44.setParent(scene)
floorBlock_04_44.addComponentOrReplace(gltfShape_2)
const transform_56 = new Transform({
  position: new Vector3(17, 0, 21.5),
  rotation: new Quaternion(0.004755127342809566, 0.7028152458818533, 0.004812806079315271, 0.7113402531964754),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_44.addComponentOrReplace(transform_56)
engine.addEntity(floorBlock_04_44)

const floorBlock_04_45 = new Entity()
floorBlock_04_45.setParent(scene)
floorBlock_04_45.addComponentOrReplace(gltfShape_2)
const transform_57 = new Transform({
  position: new Vector3(17, 0, 27),
  rotation: new Quaternion(0.004755127342809566, 0.7028152458818533, 0.004812806079315271, 0.7113402531964754),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_45.addComponentOrReplace(transform_57)
engine.addEntity(floorBlock_04_45)

const floorBlock_04_46 = new Entity()
floorBlock_04_46.setParent(scene)
floorBlock_04_46.addComponentOrReplace(gltfShape_2)
const transform_58 = new Transform({
  position: new Vector3(21, 0, 27),
  rotation: new Quaternion(0.004755127342809566, 0.7028152458818533, 0.004812806079315271, 0.7113402531964754),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_46.addComponentOrReplace(transform_58)
engine.addEntity(floorBlock_04_46)

const floorBlock_04_47 = new Entity()
floorBlock_04_47.setParent(scene)
floorBlock_04_47.addComponentOrReplace(gltfShape_2)
const transform_59 = new Transform({
  position: new Vector3(17, 0, 23),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_47.addComponentOrReplace(transform_59)
engine.addEntity(floorBlock_04_47)

const floorBlock_04_48 = new Entity()
floorBlock_04_48.setParent(scene)
floorBlock_04_48.addComponentOrReplace(gltfShape_2)
const transform_60 = new Transform({
  position: new Vector3(17, 0, 25),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_48.addComponentOrReplace(transform_60)
engine.addEntity(floorBlock_04_48)

const docksModuleCurve_01 = new Entity()
docksModuleCurve_01.setParent(scene)
const gltfShape_6 = new GLTFShape('models/DocksModuleCurve_01/DocksModuleCurve_01.glb')
docksModuleCurve_01.addComponentOrReplace(gltfShape_6)
const transform_61 = new Transform({
  position: new Vector3(45, 0, 33.5),
  rotation: new Quaternion(0, -0.7071067811865479, 0, 0.7071067811865479),
  scale: new Vector3(1, 1, 1)
})
docksModuleCurve_01.addComponentOrReplace(transform_61)
engine.addEntity(docksModuleCurve_01)

const docksModuleMedium_01 = new Entity()
docksModuleMedium_01.setParent(scene)
const gltfShape_7 = new GLTFShape('models/DocksModuleMedium_01/DocksModuleMedium_01.glb')
docksModuleMedium_01.addComponentOrReplace(gltfShape_7)
const transform_62 = new Transform({
  position: new Vector3(41, 0, 33.5),
  rotation: new Quaternion(0, 0.707106781186548, 0, -0.7071067811865474),
  scale: new Vector3(1, 1, 1)
})
docksModuleMedium_01.addComponentOrReplace(transform_62)
engine.addEntity(docksModuleMedium_01)

const docksModuleMedium_01_2 = new Entity()
docksModuleMedium_01_2.setParent(scene)
docksModuleMedium_01_2.addComponentOrReplace(gltfShape_7)
const transform_63 = new Transform({
  position: new Vector3(37, 0, 33.5),
  rotation: new Quaternion(0, -0.7071067811865478, 0, 0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
docksModuleMedium_01_2.addComponentOrReplace(transform_63)
engine.addEntity(docksModuleMedium_01_2)

const waterPatchFull_01 = new Entity()
waterPatchFull_01.setParent(scene)
const gltfShape_8 = new GLTFShape('models/WaterPatchFull_01/WaterPatchFull_01.glb')
waterPatchFull_01.addComponentOrReplace(gltfShape_8)
const transform_64 = new Transform({
  position: new Vector3(8, 0, 48),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01.addComponentOrReplace(transform_64)
engine.addEntity(waterPatchFull_01)

const waterPatchFull_01_2 = new Entity()
waterPatchFull_01_2.setParent(scene)
waterPatchFull_01_2.addComponentOrReplace(gltfShape_8)
const transform_65 = new Transform({
  position: new Vector3(32, 0, 48),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_2.addComponentOrReplace(transform_65)
engine.addEntity(waterPatchFull_01_2)

const waterPatchFull_01_3 = new Entity()
waterPatchFull_01_3.setParent(scene)
waterPatchFull_01_3.addComponentOrReplace(gltfShape_8)
const transform_66 = new Transform({
  position: new Vector3(40, 0, 48),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_3.addComponentOrReplace(transform_66)
engine.addEntity(waterPatchFull_01_3)

const waterPatchFull_01_4 = new Entity()
waterPatchFull_01_4.setParent(scene)
waterPatchFull_01_4.addComponentOrReplace(gltfShape_8)
const transform_67 = new Transform({
  position: new Vector3(48, 0, 40),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_4.addComponentOrReplace(transform_67)
engine.addEntity(waterPatchFull_01_4)

const waterPatchFull_01_5 = new Entity()
waterPatchFull_01_5.setParent(scene)
waterPatchFull_01_5.addComponentOrReplace(gltfShape_8)
const transform_68 = new Transform({
  position: new Vector3(16.049601151021932, 0, 47.9896819357642),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_5.addComponentOrReplace(transform_68)
engine.addEntity(waterPatchFull_01_5)

const waterPatchFull_01_6 = new Entity()
waterPatchFull_01_6.setParent(scene)
waterPatchFull_01_6.addComponentOrReplace(gltfShape_8)
const transform_69 = new Transform({
  position: new Vector3(24.02580337473432, 0, 47.9863487054828),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_6.addComponentOrReplace(transform_69)
engine.addEntity(waterPatchFull_01_6)

const docksModuleStairs_01 = new Entity()
docksModuleStairs_01.setParent(scene)
const gltfShape_9 = new GLTFShape('models/DocksModuleStairs_01/DocksModuleStairs_01.glb')
docksModuleStairs_01.addComponentOrReplace(gltfShape_9)
const transform_70 = new Transform({
  position: new Vector3(47, 0, 30),
  rotation: new Quaternion(0, -1.0000000000000007, 0, 9.71445146547012e-17),
  scale: new Vector3(1, 1, 1)
})
docksModuleStairs_01.addComponentOrReplace(transform_70)
engine.addEntity(docksModuleStairs_01)



const ship_01 = new Entity()
ship_01.setParent(scene)
const gltfShape_10 = new GLTFShape('models/Ship_01/Ship_01.glb')
ship_01.addComponentOrReplace(gltfShape_10)
const transform_71 = new Transform({
  position: new Vector3(40.5, 0, 38),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865479),
  scale: new Vector3(1, 1, 1)
})
ship_01.addComponentOrReplace(transform_71)
engine.addEntity(ship_01)
hud.attachToEntity(ship_01)
hud.setDefaultParent(scene)



// spawnGLTFX modules
let ship2Shape = new GLTFShape('models/Ship_01/Ship_01.glb')
let ship2 = spawnGltfX(ship2Shape, 60,0,60,	0,0,0,		1,1,1)  

const waterPatchFull_01_7 = new Entity()
waterPatchFull_01_7.setParent(scene)
waterPatchFull_01_7.addComponentOrReplace(gltfShape_8)
const transform_72 = new Transform({
  position: new Vector3(32, 0, 40),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_7.addComponentOrReplace(transform_72)
engine.addEntity(waterPatchFull_01_7)

const waterPatchFull_01_8 = new Entity()
waterPatchFull_01_8.setParent(scene)
waterPatchFull_01_8.addComponentOrReplace(gltfShape_8)
const transform_73 = new Transform({
  position: new Vector3(48, 0, 48),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_8.addComponentOrReplace(transform_73)
engine.addEntity(waterPatchFull_01_8)

const waterPatchFull_01_9 = new Entity()
waterPatchFull_01_9.setParent(scene)
waterPatchFull_01_9.addComponentOrReplace(gltfShape_8)
const transform_74 = new Transform({
  position: new Vector3(8, 0, 40),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_9.addComponentOrReplace(transform_74)
engine.addEntity(waterPatchFull_01_9)

const waterPatchFull_01_10 = new Entity()
waterPatchFull_01_10.setParent(scene)
waterPatchFull_01_10.addComponentOrReplace(gltfShape_8)
const transform_75 = new Transform({
  position: new Vector3(40, 0, 40),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_10.addComponentOrReplace(transform_75)
engine.addEntity(waterPatchFull_01_10)

const waterPatchFull_01_11 = new Entity()
waterPatchFull_01_11.setParent(scene)
waterPatchFull_01_11.addComponentOrReplace(gltfShape_8)
const transform_76 = new Transform({
  position: new Vector3(24, 0, 40),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_11.addComponentOrReplace(transform_76)
engine.addEntity(waterPatchFull_01_11)

const waterPatchFull_01_12 = new Entity()
waterPatchFull_01_12.setParent(scene)
waterPatchFull_01_12.addComponentOrReplace(gltfShape_8)
const transform_77 = new Transform({
  position: new Vector3(16, 0, 40),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_12.addComponentOrReplace(transform_77)
engine.addEntity(waterPatchFull_01_12)

const lilyPad_01 = new Entity()
lilyPad_01.setParent(scene)
const gltfShape_11 = new GLTFShape('models/LilyPad_01/LilyPad_01.glb')
lilyPad_01.addComponentOrReplace(gltfShape_11)
const transform_78 = new Transform({
  position: new Vector3(24.5, 0, 35.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
lilyPad_01.addComponentOrReplace(transform_78)
engine.addEntity(lilyPad_01)

const rockArc_01 = new Entity()
rockArc_01.setParent(scene)
const gltfShape_12 = new GLTFShape('models/RockArc_01/RockArc_01.glb')
rockArc_01.addComponentOrReplace(gltfShape_12)
const transform_79 = new Transform({
  position: new Vector3(7.5, 0, 44),
  rotation: new Quaternion(0, -0.38268343236508984, 0, 0.9238795325112868),
  scale: new Vector3(1, 1, 1)
})
rockArc_01.addComponentOrReplace(transform_79)
engine.addEntity(rockArc_01)

const lampPost_04 = new Entity()
lampPost_04.setParent(scene)
const gltfShape_13 = new GLTFShape('models/LampPost_04/LampPost_04.glb')
lampPost_04.addComponentOrReplace(gltfShape_13)
const transform_80 = new Transform({
  position: new Vector3(33.5, 0, 30),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
lampPost_04.addComponentOrReplace(transform_80)
engine.addEntity(lampPost_04)

const floorBlock_04_49 = new Entity()
floorBlock_04_49.setParent(scene)
floorBlock_04_49.addComponentOrReplace(gltfShape_2)
const transform_81 = new Transform({
  position: new Vector3(49, 0, 27),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_49.addComponentOrReplace(transform_81)
engine.addEntity(floorBlock_04_49)

const floorBlock_04_50 = new Entity()
floorBlock_04_50.setParent(scene)
floorBlock_04_50.addComponentOrReplace(gltfShape_2)
const transform_82 = new Transform({
  position: new Vector3(47, 0, 27),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_50.addComponentOrReplace(transform_82)
engine.addEntity(floorBlock_04_50)

const floorBlock_04_51 = new Entity()
floorBlock_04_51.setParent(scene)
floorBlock_04_51.addComponentOrReplace(gltfShape_2)
const transform_83 = new Transform({
  position: new Vector3(45, 0, 27),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865475),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_51.addComponentOrReplace(transform_83)
engine.addEntity(floorBlock_04_51)

const floorBlock_04_52 = new Entity()
floorBlock_04_52.setParent(scene)
floorBlock_04_52.addComponentOrReplace(gltfShape_2)
const transform_84 = new Transform({
  position: new Vector3(43, 0, 27),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865475),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_52.addComponentOrReplace(transform_84)
engine.addEntity(floorBlock_04_52)

const floorBlock_04_53 = new Entity()
floorBlock_04_53.setParent(scene)
floorBlock_04_53.addComponentOrReplace(gltfShape_2)
const transform_85 = new Transform({
  position: new Vector3(41, 0, 27),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865475),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_53.addComponentOrReplace(transform_85)
engine.addEntity(floorBlock_04_53)

const floorBlock_04_54 = new Entity()
floorBlock_04_54.setParent(scene)
floorBlock_04_54.addComponentOrReplace(gltfShape_2)
const transform_86 = new Transform({
  position: new Vector3(39, 0, 27),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865475),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_54.addComponentOrReplace(transform_86)
engine.addEntity(floorBlock_04_54)

const floorBlock_04_55 = new Entity()
floorBlock_04_55.setParent(scene)
floorBlock_04_55.addComponentOrReplace(gltfShape_2)
const transform_87 = new Transform({
  position: new Vector3(37, 0, 27),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865475),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_55.addComponentOrReplace(transform_87)
engine.addEntity(floorBlock_04_55)

const floorBlock_04_56 = new Entity()
floorBlock_04_56.setParent(scene)
floorBlock_04_56.addComponentOrReplace(gltfShape_2)
const transform_88 = new Transform({
  position: new Vector3(35, 0, 27),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865475),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_56.addComponentOrReplace(transform_88)
engine.addEntity(floorBlock_04_56)

const fenceStoneTallLarge_01 = new Entity()
fenceStoneTallLarge_01.setParent(scene)
const gltfShape_14 = new GLTFShape('models/FenceStoneTallLarge_01/FenceStoneTallLarge_01.glb')
fenceStoneTallLarge_01.addComponentOrReplace(gltfShape_14)
const transform_89 = new Transform({
  position: new Vector3(1, 0, 32),
  rotation: new Quaternion(0, -0.7071067811865479, 0, 0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
fenceStoneTallLarge_01.addComponentOrReplace(transform_89)
engine.addEntity(fenceStoneTallLarge_01)

const fenceStoneTallLarge_01_2 = new Entity()
fenceStoneTallLarge_01_2.setParent(scene)
fenceStoneTallLarge_01_2.addComponentOrReplace(gltfShape_14)
const transform_90 = new Transform({
  position: new Vector3(41, 0, 32),
  rotation: new Quaternion(0, -0.7071067811865479, 0, 0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
fenceStoneTallLarge_01_2.addComponentOrReplace(transform_90)
engine.addEntity(fenceStoneTallLarge_01_2)

const fenceStoneTallLarge_01_3 = new Entity()
fenceStoneTallLarge_01_3.setParent(scene)
fenceStoneTallLarge_01_3.addComponentOrReplace(gltfShape_14)
const transform_91 = new Transform({
  position: new Vector3(36, 0, 32),
  rotation: new Quaternion(0, -0.7071067811865479, 0, 0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
fenceStoneTallLarge_01_3.addComponentOrReplace(transform_91)
engine.addEntity(fenceStoneTallLarge_01_3)

const fenceStoneTallLarge_01_4 = new Entity()
fenceStoneTallLarge_01_4.setParent(scene)
fenceStoneTallLarge_01_4.addComponentOrReplace(gltfShape_14)
const transform_92 = new Transform({
  position: new Vector3(31, 0, 32),
  rotation: new Quaternion(0, -0.7071067811865479, 0, 0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
fenceStoneTallLarge_01_4.addComponentOrReplace(transform_92)
engine.addEntity(fenceStoneTallLarge_01_4)

const fenceStoneTallLarge_01_5 = new Entity()
fenceStoneTallLarge_01_5.setParent(scene)
fenceStoneTallLarge_01_5.addComponentOrReplace(gltfShape_14)
const transform_93 = new Transform({
  position: new Vector3(26, 0, 32),
  rotation: new Quaternion(0, -0.7071067811865479, 0, 0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
fenceStoneTallLarge_01_5.addComponentOrReplace(transform_93)
engine.addEntity(fenceStoneTallLarge_01_5)

const fenceStoneTallLarge_01_6 = new Entity()
fenceStoneTallLarge_01_6.setParent(scene)
fenceStoneTallLarge_01_6.addComponentOrReplace(gltfShape_14)
const transform_94 = new Transform({
  position: new Vector3(21, 0, 32),
  rotation: new Quaternion(0, -0.7071067811865479, 0, 0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
fenceStoneTallLarge_01_6.addComponentOrReplace(transform_94)
engine.addEntity(fenceStoneTallLarge_01_6)

const fenceStoneTallLarge_01_7 = new Entity()
fenceStoneTallLarge_01_7.setParent(scene)
fenceStoneTallLarge_01_7.addComponentOrReplace(gltfShape_14)
const transform_95 = new Transform({
  position: new Vector3(16, 0, 32),
  rotation: new Quaternion(0, -0.7071067811865479, 0, 0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
fenceStoneTallLarge_01_7.addComponentOrReplace(transform_95)
engine.addEntity(fenceStoneTallLarge_01_7)

const fenceStoneTallLarge_01_8 = new Entity()
fenceStoneTallLarge_01_8.setParent(scene)
fenceStoneTallLarge_01_8.addComponentOrReplace(gltfShape_14)
const transform_96 = new Transform({
  position: new Vector3(11, 0, 32),
  rotation: new Quaternion(0, -0.7071067811865479, 0, 0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
fenceStoneTallLarge_01_8.addComponentOrReplace(transform_96)
engine.addEntity(fenceStoneTallLarge_01_8)

const fenceStoneTallLarge_01_9 = new Entity()
fenceStoneTallLarge_01_9.setParent(scene)
fenceStoneTallLarge_01_9.addComponentOrReplace(gltfShape_14)
const transform_97 = new Transform({
  position: new Vector3(6, 0, 32),
  rotation: new Quaternion(0, -0.7071067811865479, 0, 0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
fenceStoneTallLarge_01_9.addComponentOrReplace(transform_97)
engine.addEntity(fenceStoneTallLarge_01_9)

const fenceStoneTallMedium_01 = new Entity()
fenceStoneTallMedium_01.setParent(scene)
const gltfShape_15 = new GLTFShape('models/FenceStoneTallMedium_01/FenceStoneTallMedium_01.glb')
fenceStoneTallMedium_01.addComponentOrReplace(gltfShape_15)
const transform_98 = new Transform({
  position: new Vector3(0.5, 0, 32),
  rotation: new Quaternion(0, -0.7071067811865478, 0, 0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
fenceStoneTallMedium_01.addComponentOrReplace(transform_98)
engine.addEntity(fenceStoneTallMedium_01)

const treeFir_01_8 = new Entity()
treeFir_01_8.setParent(scene)
treeFir_01_8.addComponentOrReplace(gltfShape)
const transform_99 = new Transform({
  position: new Vector3(19.5, 0, 23),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
treeFir_01_8.addComponentOrReplace(transform_99)
engine.addEntity(treeFir_01_8)

const treeFir_01_9 = new Entity()
treeFir_01_9.setParent(scene)
treeFir_01_9.addComponentOrReplace(gltfShape)
const transform_100 = new Transform({
  position: new Vector3(13, 0, 19.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
treeFir_01_9.addComponentOrReplace(transform_100)
engine.addEntity(treeFir_01_9)

const treeFir_01_10 = new Entity()
treeFir_01_10.setParent(scene)
treeFir_01_10.addComponentOrReplace(gltfShape)
const transform_101 = new Transform({
  position: new Vector3(14.5, 0, 11.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
treeFir_01_10.addComponentOrReplace(transform_101)
engine.addEntity(treeFir_01_10)

const floorBlock_03 = new Entity()
floorBlock_03.setParent(scene)
const gltfShape_16 = new GLTFShape('models/FloorBlock_03/FloorBlock_03.glb')
floorBlock_03.addComponentOrReplace(gltfShape_16)
const transform_102 = new Transform({
  position: new Vector3(38, 13, 14.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03.addComponentOrReplace(transform_102)
engine.addEntity(floorBlock_03)

const floorBlock_03_2 = new Entity()
floorBlock_03_2.setParent(scene)
floorBlock_03_2.addComponentOrReplace(gltfShape_16)
const transform_103 = new Transform({
  position: new Vector3(40, 13, 20.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_2.addComponentOrReplace(transform_103)
engine.addEntity(floorBlock_03_2)

const floorBlock_03_3 = new Entity()
floorBlock_03_3.setParent(scene)
floorBlock_03_3.addComponentOrReplace(gltfShape_16)
const transform_104 = new Transform({
  position: new Vector3(36, 13, 16.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_3.addComponentOrReplace(transform_104)
engine.addEntity(floorBlock_03_3)

const floorBlock_03_4 = new Entity()
floorBlock_03_4.setParent(scene)
floorBlock_03_4.addComponentOrReplace(gltfShape_16)
const transform_105 = new Transform({
  position: new Vector3(38, 13, 16.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_4.addComponentOrReplace(transform_105)
engine.addEntity(floorBlock_03_4)

const floorBlock_03_5 = new Entity()
floorBlock_03_5.setParent(scene)
floorBlock_03_5.addComponentOrReplace(gltfShape_16)
const transform_106 = new Transform({
  position: new Vector3(38, 13, 18.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_5.addComponentOrReplace(transform_106)
engine.addEntity(floorBlock_03_5)

const floorBlock_03_6 = new Entity()
floorBlock_03_6.setParent(scene)
floorBlock_03_6.addComponentOrReplace(gltfShape_16)
const transform_107 = new Transform({
  position: new Vector3(40, 13, 14.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_6.addComponentOrReplace(transform_107)
engine.addEntity(floorBlock_03_6)

const floorBlock_03_7 = new Entity()
floorBlock_03_7.setParent(scene)
floorBlock_03_7.addComponentOrReplace(gltfShape_16)
const transform_108 = new Transform({
  position: new Vector3(38, 13, 8.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_7.addComponentOrReplace(transform_108)
engine.addEntity(floorBlock_03_7)

const floorBlock_03_8 = new Entity()
floorBlock_03_8.setParent(scene)
floorBlock_03_8.addComponentOrReplace(gltfShape_16)
const transform_109 = new Transform({
  position: new Vector3(36, 13, 14.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_8.addComponentOrReplace(transform_109)
engine.addEntity(floorBlock_03_8)

const floorBlock_03_9 = new Entity()
floorBlock_03_9.setParent(scene)
floorBlock_03_9.addComponentOrReplace(gltfShape_16)
const transform_110 = new Transform({
  position: new Vector3(40, 13, 16.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_9.addComponentOrReplace(transform_110)
engine.addEntity(floorBlock_03_9)

const floorBlock_03_10 = new Entity()
floorBlock_03_10.setParent(scene)
floorBlock_03_10.addComponentOrReplace(gltfShape_16)
const transform_111 = new Transform({
  position: new Vector3(40, 13, 18.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_10.addComponentOrReplace(transform_111)
engine.addEntity(floorBlock_03_10)

const floorBlock_03_11 = new Entity()
floorBlock_03_11.setParent(scene)
floorBlock_03_11.addComponentOrReplace(gltfShape_16)
const transform_112 = new Transform({
  position: new Vector3(36, 13, 12.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_11.addComponentOrReplace(transform_112)
engine.addEntity(floorBlock_03_11)

const floorBlock_03_12 = new Entity()
floorBlock_03_12.setParent(scene)
floorBlock_03_12.addComponentOrReplace(gltfShape_16)
const transform_113 = new Transform({
  position: new Vector3(38, 13, 12.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_12.addComponentOrReplace(transform_113)
engine.addEntity(floorBlock_03_12)

const floorBlock_03_13 = new Entity()
floorBlock_03_13.setParent(scene)
floorBlock_03_13.addComponentOrReplace(gltfShape_16)
const transform_114 = new Transform({
  position: new Vector3(40, 13, 12.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_13.addComponentOrReplace(transform_114)
engine.addEntity(floorBlock_03_13)

const floorBlock_03_14 = new Entity()
floorBlock_03_14.setParent(scene)
floorBlock_03_14.addComponentOrReplace(gltfShape_16)
const transform_115 = new Transform({
  position: new Vector3(42, 13, 12.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_14.addComponentOrReplace(transform_115)
engine.addEntity(floorBlock_03_14)

const floorBlock_03_15 = new Entity()
floorBlock_03_15.setParent(scene)
floorBlock_03_15.addComponentOrReplace(gltfShape_16)
const transform_116 = new Transform({
  position: new Vector3(38, 13, 10.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_15.addComponentOrReplace(transform_116)
engine.addEntity(floorBlock_03_15)

const floorBlock_03_16 = new Entity()
floorBlock_03_16.setParent(scene)
floorBlock_03_16.addComponentOrReplace(gltfShape_16)
const transform_117 = new Transform({
  position: new Vector3(40, 13, 10.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_16.addComponentOrReplace(transform_117)
engine.addEntity(floorBlock_03_16)

const floorBlock_03_17 = new Entity()
floorBlock_03_17.setParent(scene)
floorBlock_03_17.addComponentOrReplace(gltfShape_16)
const transform_118 = new Transform({
  position: new Vector3(42, 13, 14.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_17.addComponentOrReplace(transform_118)
engine.addEntity(floorBlock_03_17)

const floorBlock_03_18 = new Entity()
floorBlock_03_18.setParent(scene)
floorBlock_03_18.addComponentOrReplace(gltfShape_16)
const transform_119 = new Transform({
  position: new Vector3(42, 13, 16.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_18.addComponentOrReplace(transform_119)
engine.addEntity(floorBlock_03_18)

const floorBlock_03_19 = new Entity()
floorBlock_03_19.setParent(scene)
floorBlock_03_19.addComponentOrReplace(gltfShape_16)
const transform_120 = new Transform({
  position: new Vector3(42, 13, 18.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_19.addComponentOrReplace(transform_120)
engine.addEntity(floorBlock_03_19)

const floorBlock_03_20 = new Entity()
floorBlock_03_20.setParent(scene)
floorBlock_03_20.addComponentOrReplace(gltfShape_16)
const transform_121 = new Transform({
  position: new Vector3(44, 13, 10.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_20.addComponentOrReplace(transform_121)
engine.addEntity(floorBlock_03_20)

const floorBlock_03_21 = new Entity()
floorBlock_03_21.setParent(scene)
floorBlock_03_21.addComponentOrReplace(gltfShape_16)
const transform_122 = new Transform({
  position: new Vector3(46, 13, 6.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_21.addComponentOrReplace(transform_122)
engine.addEntity(floorBlock_03_21)

const floorBlock_03_22 = new Entity()
floorBlock_03_22.setParent(scene)
floorBlock_03_22.addComponentOrReplace(gltfShape_16)
const transform_123 = new Transform({
  position: new Vector3(42, 13, 10.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_22.addComponentOrReplace(transform_123)
engine.addEntity(floorBlock_03_22)

const floorBlock_03_23 = new Entity()
floorBlock_03_23.setParent(scene)
floorBlock_03_23.addComponentOrReplace(gltfShape_16)
const transform_124 = new Transform({
  position: new Vector3(44, 13, 12.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_23.addComponentOrReplace(transform_124)
engine.addEntity(floorBlock_03_23)

const floorBlock_03_24 = new Entity()
floorBlock_03_24.setParent(scene)
floorBlock_03_24.addComponentOrReplace(gltfShape_16)
const transform_125 = new Transform({
  position: new Vector3(44, 13, 14.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_24.addComponentOrReplace(transform_125)
engine.addEntity(floorBlock_03_24)

const floorBlock_03_25 = new Entity()
floorBlock_03_25.setParent(scene)
floorBlock_03_25.addComponentOrReplace(gltfShape_16)
const transform_126 = new Transform({
  position: new Vector3(44, 13, 16.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_25.addComponentOrReplace(transform_126)
engine.addEntity(floorBlock_03_25)

const floorBlock_03_26 = new Entity()
floorBlock_03_26.setParent(scene)
floorBlock_03_26.addComponentOrReplace(gltfShape_16)
const transform_127 = new Transform({
  position: new Vector3(44, 13, 18.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_26.addComponentOrReplace(transform_127)
engine.addEntity(floorBlock_03_26)

const floorBlock_03_27 = new Entity()
floorBlock_03_27.setParent(scene)
floorBlock_03_27.addComponentOrReplace(gltfShape_16)
const transform_128 = new Transform({
  position: new Vector3(46, 13, 18.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_27.addComponentOrReplace(transform_128)
engine.addEntity(floorBlock_03_27)

const floorBlock_03_28 = new Entity()
floorBlock_03_28.setParent(scene)
floorBlock_03_28.addComponentOrReplace(gltfShape_16)
const transform_129 = new Transform({
  position: new Vector3(46, 13, 16.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_28.addComponentOrReplace(transform_129)
engine.addEntity(floorBlock_03_28)

const floorBlock_03_29 = new Entity()
floorBlock_03_29.setParent(scene)
floorBlock_03_29.addComponentOrReplace(gltfShape_16)
const transform_130 = new Transform({
  position: new Vector3(46, 13, 14.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_29.addComponentOrReplace(transform_130)
engine.addEntity(floorBlock_03_29)

const floorBlock_03_30 = new Entity()
floorBlock_03_30.setParent(scene)
floorBlock_03_30.addComponentOrReplace(gltfShape_16)
const transform_131 = new Transform({
  position: new Vector3(46, 13, 12.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_30.addComponentOrReplace(transform_131)
engine.addEntity(floorBlock_03_30)

const floorBlock_03_31 = new Entity()
floorBlock_03_31.setParent(scene)
floorBlock_03_31.addComponentOrReplace(gltfShape_16)
const transform_132 = new Transform({
  position: new Vector3(36, 13, 18.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_31.addComponentOrReplace(transform_132)
engine.addEntity(floorBlock_03_31)

const floorBlock_03_32 = new Entity()
floorBlock_03_32.setParent(scene)
floorBlock_03_32.addComponentOrReplace(gltfShape_16)
const transform_133 = new Transform({
  position: new Vector3(42, 13, 22.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_32.addComponentOrReplace(transform_133)
engine.addEntity(floorBlock_03_32)

const floorBlock_03_33 = new Entity()
floorBlock_03_33.setParent(scene)
floorBlock_03_33.addComponentOrReplace(gltfShape_16)
const transform_134 = new Transform({
  position: new Vector3(38, 13, 20.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_33.addComponentOrReplace(transform_134)
engine.addEntity(floorBlock_03_33)

const floorBlock_03_34 = new Entity()
floorBlock_03_34.setParent(scene)
floorBlock_03_34.addComponentOrReplace(gltfShape_16)
const transform_135 = new Transform({
  position: new Vector3(36, 13, 20.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_34.addComponentOrReplace(transform_135)
engine.addEntity(floorBlock_03_34)

const floorBlock_03_35 = new Entity()
floorBlock_03_35.setParent(scene)
floorBlock_03_35.addComponentOrReplace(gltfShape_16)
const transform_136 = new Transform({
  position: new Vector3(34, 13, 6.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_35.addComponentOrReplace(transform_136)
engine.addEntity(floorBlock_03_35)

const floorBlock_03_36 = new Entity()
floorBlock_03_36.setParent(scene)
floorBlock_03_36.addComponentOrReplace(gltfShape_16)
const transform_137 = new Transform({
  position: new Vector3(38, 13, 22.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_36.addComponentOrReplace(transform_137)
engine.addEntity(floorBlock_03_36)

const floorBlock_03_37 = new Entity()
floorBlock_03_37.setParent(scene)
floorBlock_03_37.addComponentOrReplace(gltfShape_16)
const transform_138 = new Transform({
  position: new Vector3(36, 13, 10.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_37.addComponentOrReplace(transform_138)
engine.addEntity(floorBlock_03_37)

const floorBlock_03_38 = new Entity()
floorBlock_03_38.setParent(scene)
floorBlock_03_38.addComponentOrReplace(gltfShape_16)
const transform_139 = new Transform({
  position: new Vector3(44, 13, 8.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_38.addComponentOrReplace(transform_139)
engine.addEntity(floorBlock_03_38)

const floorBlock_03_39 = new Entity()
floorBlock_03_39.setParent(scene)
floorBlock_03_39.addComponentOrReplace(gltfShape_16)
const transform_140 = new Transform({
  position: new Vector3(38, 13, 8.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_39.addComponentOrReplace(transform_140)
engine.addEntity(floorBlock_03_39)

const floorBlock_03_40 = new Entity()
floorBlock_03_40.setParent(scene)
floorBlock_03_40.addComponentOrReplace(gltfShape_16)
const transform_141 = new Transform({
  position: new Vector3(44, 13, 6.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_40.addComponentOrReplace(transform_141)
engine.addEntity(floorBlock_03_40)

const floorBlock_03_41 = new Entity()
floorBlock_03_41.setParent(scene)
floorBlock_03_41.addComponentOrReplace(gltfShape_16)
const transform_142 = new Transform({
  position: new Vector3(40, 13, 8.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_41.addComponentOrReplace(transform_142)
engine.addEntity(floorBlock_03_41)

const floorBlock_03_42 = new Entity()
floorBlock_03_42.setParent(scene)
floorBlock_03_42.addComponentOrReplace(gltfShape_16)
const transform_143 = new Transform({
  position: new Vector3(42, 13, 8.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_42.addComponentOrReplace(transform_143)
engine.addEntity(floorBlock_03_42)

const floorBlock_03_43 = new Entity()
floorBlock_03_43.setParent(scene)
floorBlock_03_43.addComponentOrReplace(gltfShape_16)
const transform_144 = new Transform({
  position: new Vector3(36, 13, 8.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_43.addComponentOrReplace(transform_144)
engine.addEntity(floorBlock_03_43)

const floorBlock_03_44 = new Entity()
floorBlock_03_44.setParent(scene)
floorBlock_03_44.addComponentOrReplace(gltfShape_16)
const transform_145 = new Transform({
  position: new Vector3(36, 13, 6.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_44.addComponentOrReplace(transform_145)
engine.addEntity(floorBlock_03_44)

const floorBlock_03_45 = new Entity()
floorBlock_03_45.setParent(scene)
floorBlock_03_45.addComponentOrReplace(gltfShape_16)
const transform_146 = new Transform({
  position: new Vector3(38, 13, 6.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_45.addComponentOrReplace(transform_146)
engine.addEntity(floorBlock_03_45)

const floorBlock_03_46 = new Entity()
floorBlock_03_46.setParent(scene)
floorBlock_03_46.addComponentOrReplace(gltfShape_16)
const transform_147 = new Transform({
  position: new Vector3(40, 13, 6.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_46.addComponentOrReplace(transform_147)
engine.addEntity(floorBlock_03_46)

const floorBlock_03_47 = new Entity()
floorBlock_03_47.setParent(scene)
floorBlock_03_47.addComponentOrReplace(gltfShape_16)
const transform_148 = new Transform({
  position: new Vector3(42, 13, 6.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_47.addComponentOrReplace(transform_148)
engine.addEntity(floorBlock_03_47)

const chineseHouse_01 = new Entity()
chineseHouse_01.setParent(scene)
const gltfShape_17 = new GLTFShape('models/ChineseHouse_01/ChineseHouse_01.glb')
chineseHouse_01.addComponentOrReplace(gltfShape_17)
const transform_149 = new Transform({
  position: new Vector3(40.5, 13, 15.5),
  rotation: new Quaternion(0, -0.7071067811865479, 0, 0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
chineseHouse_01.addComponentOrReplace(transform_149)
engine.addEntity(chineseHouse_01)

const floorBlock_03_48 = new Entity()
floorBlock_03_48.setParent(scene)
floorBlock_03_48.addComponentOrReplace(gltfShape_16)
const transform_150 = new Transform({
  position: new Vector3(40, 13, 22.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_48.addComponentOrReplace(transform_150)
engine.addEntity(floorBlock_03_48)

const floorBlock_03_49 = new Entity()
floorBlock_03_49.setParent(scene)
floorBlock_03_49.addComponentOrReplace(gltfShape_16)
const transform_151 = new Transform({
  position: new Vector3(42, 13, 20.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_49.addComponentOrReplace(transform_151)
engine.addEntity(floorBlock_03_49)

const floorBlock_03_50 = new Entity()
floorBlock_03_50.setParent(scene)
floorBlock_03_50.addComponentOrReplace(gltfShape_16)
const transform_152 = new Transform({
  position: new Vector3(44, 13, 20.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_50.addComponentOrReplace(transform_152)
engine.addEntity(floorBlock_03_50)

const floorBlock_03_51 = new Entity()
floorBlock_03_51.setParent(scene)
floorBlock_03_51.addComponentOrReplace(gltfShape_16)
const transform_153 = new Transform({
  position: new Vector3(46, 13, 20.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_51.addComponentOrReplace(transform_153)
engine.addEntity(floorBlock_03_51)

const floorBlock_03_52 = new Entity()
floorBlock_03_52.setParent(scene)
floorBlock_03_52.addComponentOrReplace(gltfShape_16)
const transform_154 = new Transform({
  position: new Vector3(46, 13, 22.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_52.addComponentOrReplace(transform_154)
engine.addEntity(floorBlock_03_52)

const floorBlock_03_53 = new Entity()
floorBlock_03_53.setParent(scene)
floorBlock_03_53.addComponentOrReplace(gltfShape_16)
const transform_155 = new Transform({
  position: new Vector3(44, 13, 22.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_53.addComponentOrReplace(transform_155)
engine.addEntity(floorBlock_03_53)

const floorBlock_03_54 = new Entity()
floorBlock_03_54.setParent(scene)
floorBlock_03_54.addComponentOrReplace(gltfShape_16)
const transform_156 = new Transform({
  position: new Vector3(46, 13, 10.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_54.addComponentOrReplace(transform_156)
engine.addEntity(floorBlock_03_54)

const floorBlock_03_55 = new Entity()
floorBlock_03_55.setParent(scene)
floorBlock_03_55.addComponentOrReplace(gltfShape_16)
const transform_157 = new Transform({
  position: new Vector3(46, 13, 8.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_55.addComponentOrReplace(transform_157)
engine.addEntity(floorBlock_03_55)

const floorBlock_03_56 = new Entity()
floorBlock_03_56.setParent(scene)
floorBlock_03_56.addComponentOrReplace(gltfShape_16)
const transform_158 = new Transform({
  position: new Vector3(36, 13, 22.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_56.addComponentOrReplace(transform_158)
engine.addEntity(floorBlock_03_56)

const floorBlock_03_57 = new Entity()
floorBlock_03_57.setParent(scene)
floorBlock_03_57.addComponentOrReplace(gltfShape_16)
const transform_159 = new Transform({
  position: new Vector3(34, 13, 22.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_57.addComponentOrReplace(transform_159)
engine.addEntity(floorBlock_03_57)

const floorBlock_03_58 = new Entity()
floorBlock_03_58.setParent(scene)
floorBlock_03_58.addComponentOrReplace(gltfShape_16)
const transform_160 = new Transform({
  position: new Vector3(34, 13, 20.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_58.addComponentOrReplace(transform_160)
engine.addEntity(floorBlock_03_58)

const floorBlock_03_59 = new Entity()
floorBlock_03_59.setParent(scene)
floorBlock_03_59.addComponentOrReplace(gltfShape_16)
const transform_161 = new Transform({
  position: new Vector3(34, 13, 18.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_59.addComponentOrReplace(transform_161)
engine.addEntity(floorBlock_03_59)

const floorBlock_03_60 = new Entity()
floorBlock_03_60.setParent(scene)
floorBlock_03_60.addComponentOrReplace(gltfShape_16)
const transform_162 = new Transform({
  position: new Vector3(34, 13, 16.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_60.addComponentOrReplace(transform_162)
engine.addEntity(floorBlock_03_60)

const floorBlock_03_61 = new Entity()
floorBlock_03_61.setParent(scene)
floorBlock_03_61.addComponentOrReplace(gltfShape_16)
const transform_163 = new Transform({
  position: new Vector3(34, 13, 14.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_61.addComponentOrReplace(transform_163)
engine.addEntity(floorBlock_03_61)

const floorBlock_03_62 = new Entity()
floorBlock_03_62.setParent(scene)
floorBlock_03_62.addComponentOrReplace(gltfShape_16)
const transform_164 = new Transform({
  position: new Vector3(34, 13, 12.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_62.addComponentOrReplace(transform_164)
engine.addEntity(floorBlock_03_62)

const floorBlock_03_63 = new Entity()
floorBlock_03_63.setParent(scene)
floorBlock_03_63.addComponentOrReplace(gltfShape_16)
const transform_165 = new Transform({
  position: new Vector3(34, 13, 10.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_63.addComponentOrReplace(transform_165)
engine.addEntity(floorBlock_03_63)

const floorBlock_03_64 = new Entity()
floorBlock_03_64.setParent(scene)
floorBlock_03_64.addComponentOrReplace(gltfShape_16)
const transform_166 = new Transform({
  position: new Vector3(34, 13, 8.5),
  rotation: new Quaternion(0, 0, -1.3877787807814457e-16, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_03_64.addComponentOrReplace(transform_166)
engine.addEntity(floorBlock_03_64)

const floorBlock_04_57 = new Entity()
floorBlock_04_57.setParent(scene)
floorBlock_04_57.addComponentOrReplace(gltfShape_2)
const transform_167 = new Transform({
  position: new Vector3(27, 9.5, 14.5),
  rotation: new Quaternion(0, 0, 0.2902846772544624, 0.9569403357322089),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_57.addComponentOrReplace(transform_167)
engine.addEntity(floorBlock_04_57)

const floorBlock_04_58 = new Entity()
floorBlock_04_58.setParent(scene)
floorBlock_04_58.addComponentOrReplace(gltfShape_2)
const transform_168 = new Transform({
  position: new Vector3(30, 11.5, 16.5),
  rotation: new Quaternion(0, 0, 0.2902846772544624, 0.9569403357322089),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_58.addComponentOrReplace(transform_168)
engine.addEntity(floorBlock_04_58)

const floorBlock_04_59 = new Entity()
floorBlock_04_59.setParent(scene)
floorBlock_04_59.addComponentOrReplace(gltfShape_2)
const transform_169 = new Transform({
  position: new Vector3(30, 11.5, 14.5),
  rotation: new Quaternion(0, 0, 0.2902846772544624, 0.9569403357322089),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_59.addComponentOrReplace(transform_169)
engine.addEntity(floorBlock_04_59)

const floorBlock_04_60 = new Entity()
floorBlock_04_60.setParent(scene)
floorBlock_04_60.addComponentOrReplace(gltfShape_2)
const transform_170 = new Transform({
  position: new Vector3(28.5, 10.5, 14.5),
  rotation: new Quaternion(0, 0, 0.2902846772544624, 0.9569403357322089),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_60.addComponentOrReplace(transform_170)
engine.addEntity(floorBlock_04_60)

const floorBlock_04_61 = new Entity()
floorBlock_04_61.setParent(scene)
floorBlock_04_61.addComponentOrReplace(gltfShape_2)
const transform_171 = new Transform({
  position: new Vector3(25.5, 8.5, 14.5),
  rotation: new Quaternion(0, 0, 0.2902846772544624, 0.9569403357322089),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_61.addComponentOrReplace(transform_171)
engine.addEntity(floorBlock_04_61)

const floorBlock_04_62 = new Entity()
floorBlock_04_62.setParent(scene)
floorBlock_04_62.addComponentOrReplace(gltfShape_2)
const transform_172 = new Transform({
  position: new Vector3(28.5, 10.5, 16.5),
  rotation: new Quaternion(0, 0, 0.2902846772544624, 0.9569403357322089),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_62.addComponentOrReplace(transform_172)
engine.addEntity(floorBlock_04_62)

const floorBlock_04_63 = new Entity()
floorBlock_04_63.setParent(scene)
floorBlock_04_63.addComponentOrReplace(gltfShape_2)
const transform_173 = new Transform({
  position: new Vector3(24, 7.5, 14.5),
  rotation: new Quaternion(0, 0, 0.2902846772544624, 0.9569403357322089),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_63.addComponentOrReplace(transform_173)
engine.addEntity(floorBlock_04_63)

const floorBlock_04_64 = new Entity()
floorBlock_04_64.setParent(scene)
floorBlock_04_64.addComponentOrReplace(gltfShape_2)
const transform_174 = new Transform({
  position: new Vector3(27, 9.5, 16.5),
  rotation: new Quaternion(0, 0, 0.2902846772544624, 0.9569403357322089),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_64.addComponentOrReplace(transform_174)
engine.addEntity(floorBlock_04_64)

const floorBlock_04_65 = new Entity()
floorBlock_04_65.setParent(scene)
floorBlock_04_65.addComponentOrReplace(gltfShape_2)
const transform_175 = new Transform({
  position: new Vector3(22.5, 6.5, 14.5),
  rotation: new Quaternion(0, 0, 0.2902846772544624, 0.9569403357322089),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_65.addComponentOrReplace(transform_175)
engine.addEntity(floorBlock_04_65)

const floorBlock_04_66 = new Entity()
floorBlock_04_66.setParent(scene)
floorBlock_04_66.addComponentOrReplace(gltfShape_2)
const transform_176 = new Transform({
  position: new Vector3(25.5, 8.5, 16.5),
  rotation: new Quaternion(0, 0, 0.2902846772544624, 0.9569403357322089),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_66.addComponentOrReplace(transform_176)
engine.addEntity(floorBlock_04_66)

const floorBlock_04_67 = new Entity()
floorBlock_04_67.setParent(scene)
floorBlock_04_67.addComponentOrReplace(gltfShape_2)
const transform_177 = new Transform({
  position: new Vector3(21, 5.5, 14.5),
  rotation: new Quaternion(0, 0, 0.2902846772544624, 0.9569403357322089),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_67.addComponentOrReplace(transform_177)
engine.addEntity(floorBlock_04_67)

const floorBlock_04_68 = new Entity()
floorBlock_04_68.setParent(scene)
floorBlock_04_68.addComponentOrReplace(gltfShape_2)
const transform_178 = new Transform({
  position: new Vector3(24, 7.5, 16.5),
  rotation: new Quaternion(0, 0, 0.2902846772544624, 0.9569403357322089),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_68.addComponentOrReplace(transform_178)
engine.addEntity(floorBlock_04_68)

const floorBlock_04_69 = new Entity()
floorBlock_04_69.setParent(scene)
floorBlock_04_69.addComponentOrReplace(gltfShape_2)
const transform_179 = new Transform({
  position: new Vector3(19.5, 4.5, 14.5),
  rotation: new Quaternion(0, 0, 0.2902846772544624, 0.9569403357322089),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_69.addComponentOrReplace(transform_179)
engine.addEntity(floorBlock_04_69)

const floorBlock_04_70 = new Entity()
floorBlock_04_70.setParent(scene)
floorBlock_04_70.addComponentOrReplace(gltfShape_2)
const transform_180 = new Transform({
  position: new Vector3(22.5, 6.5, 16.5),
  rotation: new Quaternion(0, 0, 0.2902846772544624, 0.9569403357322089),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_70.addComponentOrReplace(transform_180)
engine.addEntity(floorBlock_04_70)

const floorBlock_04_71 = new Entity()
floorBlock_04_71.setParent(scene)
floorBlock_04_71.addComponentOrReplace(gltfShape_2)
const transform_181 = new Transform({
  position: new Vector3(13.5, 0.5, 14.5),
  rotation: new Quaternion(0, 0, 0.2902846772544624, 0.9569403357322089),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_71.addComponentOrReplace(transform_181)
engine.addEntity(floorBlock_04_71)

const floorBlock_04_72 = new Entity()
floorBlock_04_72.setParent(scene)
floorBlock_04_72.addComponentOrReplace(gltfShape_2)
const transform_182 = new Transform({
  position: new Vector3(21, 5.5, 16.5),
  rotation: new Quaternion(0, 0, 0.2902846772544624, 0.9569403357322089),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_72.addComponentOrReplace(transform_182)
engine.addEntity(floorBlock_04_72)

const floorBlock_04_73 = new Entity()
floorBlock_04_73.setParent(scene)
floorBlock_04_73.addComponentOrReplace(gltfShape_2)
const transform_183 = new Transform({
  position: new Vector3(19.5, 4.5, 16.5),
  rotation: new Quaternion(0, 0, 0.2902846772544624, 0.9569403357322089),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_73.addComponentOrReplace(transform_183)
engine.addEntity(floorBlock_04_73)

const floorBlock_04_74 = new Entity()
floorBlock_04_74.setParent(scene)
floorBlock_04_74.addComponentOrReplace(gltfShape_2)
const transform_184 = new Transform({
  position: new Vector3(18, 3.5, 14.5),
  rotation: new Quaternion(0, 0, 0.2902846772544624, 0.9569403357322089),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_74.addComponentOrReplace(transform_184)
engine.addEntity(floorBlock_04_74)

const floorBlock_04_75 = new Entity()
floorBlock_04_75.setParent(scene)
floorBlock_04_75.addComponentOrReplace(gltfShape_2)
const transform_185 = new Transform({
  position: new Vector3(18, 3.5, 16.5),
  rotation: new Quaternion(0, 0, 0.2902846772544624, 0.9569403357322089),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_75.addComponentOrReplace(transform_185)
engine.addEntity(floorBlock_04_75)

const floorBlock_04_76 = new Entity()
floorBlock_04_76.setParent(scene)
floorBlock_04_76.addComponentOrReplace(gltfShape_2)
const transform_186 = new Transform({
  position: new Vector3(16.5, 2.5, 16.5),
  rotation: new Quaternion(0, 0, 0.2902846772544624, 0.9569403357322089),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_76.addComponentOrReplace(transform_186)
engine.addEntity(floorBlock_04_76)

const floorBlock_04_77 = new Entity()
floorBlock_04_77.setParent(scene)
floorBlock_04_77.addComponentOrReplace(gltfShape_2)
const transform_187 = new Transform({
  position: new Vector3(16.5, 2.5, 14.5),
  rotation: new Quaternion(0, 0, 0.2902846772544624, 0.9569403357322089),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_77.addComponentOrReplace(transform_187)
engine.addEntity(floorBlock_04_77)

const floorBlock_04_78 = new Entity()
floorBlock_04_78.setParent(scene)
floorBlock_04_78.addComponentOrReplace(gltfShape_2)
const transform_188 = new Transform({
  position: new Vector3(15, 1.5, 14.5),
  rotation: new Quaternion(0, 0, 0.2902846772544624, 0.9569403357322089),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_78.addComponentOrReplace(transform_188)
engine.addEntity(floorBlock_04_78)

const floorBlock_04_79 = new Entity()
floorBlock_04_79.setParent(scene)
floorBlock_04_79.addComponentOrReplace(gltfShape_2)
const transform_189 = new Transform({
  position: new Vector3(15, 1.5, 16.5),
  rotation: new Quaternion(0, 0, 0.2902846772544624, 0.9569403357322089),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_79.addComponentOrReplace(transform_189)
engine.addEntity(floorBlock_04_79)

const floorBlock_04_80 = new Entity()
floorBlock_04_80.setParent(scene)
floorBlock_04_80.addComponentOrReplace(gltfShape_2)
const transform_190 = new Transform({
  position: new Vector3(13.5, 0.5, 16.5),
  rotation: new Quaternion(0, 0, 0.2902846772544624, 0.9569403357322089),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_80.addComponentOrReplace(transform_190)
engine.addEntity(floorBlock_04_80)

const floorBlock_04_81 = new Entity()
floorBlock_04_81.setParent(scene)
floorBlock_04_81.addComponentOrReplace(gltfShape_2)
const transform_191 = new Transform({
  position: new Vector3(11.5, 0, 18.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_81.addComponentOrReplace(transform_191)
engine.addEntity(floorBlock_04_81)

const floorBlock_04_82 = new Entity()
floorBlock_04_82.setParent(scene)
floorBlock_04_82.addComponentOrReplace(gltfShape_2)
const transform_192 = new Transform({
  position: new Vector3(8.5, 0, 15),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_82.addComponentOrReplace(transform_192)
engine.addEntity(floorBlock_04_82)

const floorBlock_04_83 = new Entity()
floorBlock_04_83.setParent(scene)
floorBlock_04_83.addComponentOrReplace(gltfShape_2)
const transform_193 = new Transform({
  position: new Vector3(10, 0, 16.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_83.addComponentOrReplace(transform_193)
engine.addEntity(floorBlock_04_83)

const floorBlock_04_84 = new Entity()
floorBlock_04_84.setParent(scene)
floorBlock_04_84.addComponentOrReplace(gltfShape_2)
const transform_194 = new Transform({
  position: new Vector3(11.5, 0, 20),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_84.addComponentOrReplace(transform_194)
engine.addEntity(floorBlock_04_84)

const floorBlock_04_85 = new Entity()
floorBlock_04_85.setParent(scene)
floorBlock_04_85.addComponentOrReplace(gltfShape_2)
const transform_195 = new Transform({
  position: new Vector3(31.5, 12.5, 14.5),
  rotation: new Quaternion(0, 0, 0.2902846772544624, 0.9569403357322089),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_85.addComponentOrReplace(transform_195)
engine.addEntity(floorBlock_04_85)

const floorBlock_04_86 = new Entity()
floorBlock_04_86.setParent(scene)
floorBlock_04_86.addComponentOrReplace(gltfShape_2)
const transform_196 = new Transform({
  position: new Vector3(33, 13, 14.5),
  rotation: new Quaternion(0, 0, 2.7755575615628914e-17, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_86.addComponentOrReplace(transform_196)
engine.addEntity(floorBlock_04_86)

const floorBlock_04_87 = new Entity()
floorBlock_04_87.setParent(scene)
floorBlock_04_87.addComponentOrReplace(gltfShape_2)
const transform_197 = new Transform({
  position: new Vector3(31.5, 12.5, 16.5),
  rotation: new Quaternion(0, 0, 0.2902846772544624, 0.9569403357322089),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_87.addComponentOrReplace(transform_197)
engine.addEntity(floorBlock_04_87)

const floorBlock_04_88 = new Entity()
floorBlock_04_88.setParent(scene)
floorBlock_04_88.addComponentOrReplace(gltfShape_2)
const transform_198 = new Transform({
  position: new Vector3(33, 13, 16.5),
  rotation: new Quaternion(0, 0, 2.7755575615628914e-17, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_88.addComponentOrReplace(transform_198)
engine.addEntity(floorBlock_04_88)

const fenceIronLarge_01 = new Entity()
fenceIronLarge_01.setParent(scene)
const gltfShape_18 = new GLTFShape('models/FenceIronLarge_01/FenceIronLarge_01.glb')
fenceIronLarge_01.addComponentOrReplace(gltfShape_18)
const transform_199 = new Transform({
  position: new Vector3(33, 13, 13.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
fenceIronLarge_01.addComponentOrReplace(transform_199)
engine.addEntity(fenceIronLarge_01)

const fenceIronLarge_01_2 = new Entity()
fenceIronLarge_01_2.setParent(scene)
fenceIronLarge_01_2.addComponentOrReplace(gltfShape_18)
const transform_200 = new Transform({
  position: new Vector3(38, 13, 23.5),
  rotation: new Quaternion(0, -0.7071067811865478, 0, 0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
fenceIronLarge_01_2.addComponentOrReplace(transform_200)
engine.addEntity(fenceIronLarge_01_2)

const fenceIronLarge_01_3 = new Entity()
fenceIronLarge_01_3.setParent(scene)
fenceIronLarge_01_3.addComponentOrReplace(gltfShape_18)
const transform_201 = new Transform({
  position: new Vector3(33, 13, 17.5),
  rotation: new Quaternion(0, -1.0000000000000007, 0, 9.71445146547012e-17),
  scale: new Vector3(1, 1, 1)
})
fenceIronLarge_01_3.addComponentOrReplace(transform_201)
engine.addEntity(fenceIronLarge_01_3)

const fenceIronLarge_01_4 = new Entity()
fenceIronLarge_01_4.setParent(scene)
fenceIronLarge_01_4.addComponentOrReplace(gltfShape_18)
const transform_202 = new Transform({
  position: new Vector3(33, 13, 23.5),
  rotation: new Quaternion(0, -6.938893903907228e-17, 0, 0.9999999999999998),
  scale: new Vector3(1, 1, 1)
})
fenceIronLarge_01_4.addComponentOrReplace(transform_202)
engine.addEntity(fenceIronLarge_01_4)

const fenceIronLarge_01_5 = new Entity()
fenceIronLarge_01_5.setParent(scene)
fenceIronLarge_01_5.addComponentOrReplace(gltfShape_18)
const transform_203 = new Transform({
  position: new Vector3(33, 13, 23.5),
  rotation: new Quaternion(0, -0.7071067811865478, 0, 0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
fenceIronLarge_01_5.addComponentOrReplace(transform_203)
engine.addEntity(fenceIronLarge_01_5)

const fenceIronLarge_01_6 = new Entity()
fenceIronLarge_01_6.setParent(scene)
fenceIronLarge_01_6.addComponentOrReplace(gltfShape_18)
const transform_204 = new Transform({
  position: new Vector3(47, 13, 23.5),
  rotation: new Quaternion(0, 0.7071067811865474, 0, 0.7071067811865474),
  scale: new Vector3(1, 1, 1)
})
fenceIronLarge_01_6.addComponentOrReplace(transform_204)
engine.addEntity(fenceIronLarge_01_6)

const fenceIronLarge_01_7 = new Entity()
fenceIronLarge_01_7.setParent(scene)
fenceIronLarge_01_7.addComponentOrReplace(gltfShape_18)
const transform_205 = new Transform({
  position: new Vector3(33, 13, 10.5),
  rotation: new Quaternion(0, -2.7755575615628914e-17, 0, 1),
  scale: new Vector3(1, 1, 1)
})
fenceIronLarge_01_7.addComponentOrReplace(transform_205)
engine.addEntity(fenceIronLarge_01_7)

const fenceIronLarge_01_8 = new Entity()
fenceIronLarge_01_8.setParent(scene)
fenceIronLarge_01_8.addComponentOrReplace(gltfShape_18)
const transform_206 = new Transform({
  position: new Vector3(47, 13, 5.5),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
fenceIronLarge_01_8.addComponentOrReplace(transform_206)
engine.addEntity(fenceIronLarge_01_8)

const fenceIronLarge_01_9 = new Entity()
fenceIronLarge_01_9.setParent(scene)
fenceIronLarge_01_9.addComponentOrReplace(gltfShape_18)
const transform_207 = new Transform({
  position: new Vector3(33, 13, 5.5),
  rotation: new Quaternion(0, -0.7071067811865478, 0, 0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
fenceIronLarge_01_9.addComponentOrReplace(transform_207)
engine.addEntity(fenceIronLarge_01_9)

const fenceIronLarge_01_10 = new Entity()
fenceIronLarge_01_10.setParent(scene)
fenceIronLarge_01_10.addComponentOrReplace(gltfShape_18)
const transform_208 = new Transform({
  position: new Vector3(38, 13, 5.5),
  rotation: new Quaternion(0, -0.7071067811865478, 0, 0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
fenceIronLarge_01_10.addComponentOrReplace(transform_208)
engine.addEntity(fenceIronLarge_01_10)

const fenceIronLarge_01_11 = new Entity()
fenceIronLarge_01_11.setParent(scene)
fenceIronLarge_01_11.addComponentOrReplace(gltfShape_18)
const transform_209 = new Transform({
  position: new Vector3(47, 13, 18.5),
  rotation: new Quaternion(0, 1, 0, 2.7755575615628914e-16),
  scale: new Vector3(1, 1, 1)
})
fenceIronLarge_01_11.addComponentOrReplace(transform_209)
engine.addEntity(fenceIronLarge_01_11)

const fenceIronLarge_01_12 = new Entity()
fenceIronLarge_01_12.setParent(scene)
fenceIronLarge_01_12.addComponentOrReplace(gltfShape_18)
const transform_210 = new Transform({
  position: new Vector3(47, 13, 5.5),
  rotation: new Quaternion(0, 1, 0, 2.7755575615628914e-16),
  scale: new Vector3(1, 1, 1)
})
fenceIronLarge_01_12.addComponentOrReplace(transform_210)
engine.addEntity(fenceIronLarge_01_12)

const fenceIronLarge_01_13 = new Entity()
fenceIronLarge_01_13.setParent(scene)
fenceIronLarge_01_13.addComponentOrReplace(gltfShape_18)
const transform_211 = new Transform({
  position: new Vector3(47, 13, 10.5),
  rotation: new Quaternion(0, 1, 0, 2.7755575615628914e-16),
  scale: new Vector3(1, 1, 1)
})
fenceIronLarge_01_13.addComponentOrReplace(transform_211)
engine.addEntity(fenceIronLarge_01_13)

const fenceIronLarge_01_14 = new Entity()
fenceIronLarge_01_14.setParent(scene)
fenceIronLarge_01_14.addComponentOrReplace(gltfShape_18)
const transform_212 = new Transform({
  position: new Vector3(47, 13, 15.5),
  rotation: new Quaternion(0, 1, 0, 2.7755575615628914e-16),
  scale: new Vector3(1, 1, 1)
})
fenceIronLarge_01_14.addComponentOrReplace(transform_212)
engine.addEntity(fenceIronLarge_01_14)

const chineseLantern_05 = new Entity()
chineseLantern_05.setParent(scene)
const gltfShape_19 = new GLTFShape('models/ChineseLantern_05/ChineseLantern_05.glb')
chineseLantern_05.addComponentOrReplace(gltfShape_19)
const transform_213 = new Transform({
  position: new Vector3(33.5, 4, 28.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
chineseLantern_05.addComponentOrReplace(transform_213)
engine.addEntity(chineseLantern_05)

const chineseLantern_05_2 = new Entity()
chineseLantern_05_2.setParent(scene)
chineseLantern_05_2.addComponentOrReplace(gltfShape_19)
const transform_214 = new Transform({
  position: new Vector3(31.5, 4, 25),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
chineseLantern_05_2.addComponentOrReplace(transform_214)
engine.addEntity(chineseLantern_05_2)

const chineseLantern_05_3 = new Entity()
chineseLantern_05_3.setParent(scene)
chineseLantern_05_3.addComponentOrReplace(gltfShape_19)
const transform_215 = new Transform({
  position: new Vector3(29.5, 4, 25),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
chineseLantern_05_3.addComponentOrReplace(transform_215)
engine.addEntity(chineseLantern_05_3)

const chineseLantern_05_4 = new Entity()
chineseLantern_05_4.setParent(scene)
chineseLantern_05_4.addComponentOrReplace(gltfShape_19)
const transform_216 = new Transform({
  position: new Vector3(31.5, 4, 28.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
chineseLantern_05_4.addComponentOrReplace(transform_216)
engine.addEntity(chineseLantern_05_4)

const chineseLantern_05_5 = new Entity()
chineseLantern_05_5.setParent(scene)
chineseLantern_05_5.addComponentOrReplace(gltfShape_19)
const transform_217 = new Transform({
  position: new Vector3(29.5, 4, 28.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
chineseLantern_05_5.addComponentOrReplace(transform_217)
engine.addEntity(chineseLantern_05_5)

const chineseLantern_05_6 = new Entity()
chineseLantern_05_6.setParent(scene)
chineseLantern_05_6.addComponentOrReplace(gltfShape_19)
const transform_218 = new Transform({
  position: new Vector3(33.5, 4, 25),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
chineseLantern_05_6.addComponentOrReplace(transform_218)
engine.addEntity(chineseLantern_05_6)

const chineseLantern_05_7 = new Entity()
chineseLantern_05_7.setParent(scene)
chineseLantern_05_7.addComponentOrReplace(gltfShape_19)
const transform_219 = new Transform({
  position: new Vector3(33.5, 4, 25),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
chineseLantern_05_7.addComponentOrReplace(transform_219)
engine.addEntity(chineseLantern_05_7)

const stonePath_01 = new Entity()
stonePath_01.setParent(scene)
const gltfShape_20 = new GLTFShape('models/StonePath_01/StonePath_01.glb')
stonePath_01.addComponentOrReplace(gltfShape_20)
const transform_220 = new Transform({
  position: new Vector3(37, 12, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01.addComponentOrReplace(transform_220)
engine.addEntity(stonePath_01)

const stonePath_01_2 = new Entity()
stonePath_01_2.setParent(scene)
stonePath_01_2.addComponentOrReplace(gltfShape_20)
const transform_221 = new Transform({
  position: new Vector3(39, 12, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_2.addComponentOrReplace(transform_221)
engine.addEntity(stonePath_01_2)

const stonePath_01_3 = new Entity()
stonePath_01_3.setParent(scene)
stonePath_01_3.addComponentOrReplace(gltfShape_20)
const transform_222 = new Transform({
  position: new Vector3(41, 12, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_3.addComponentOrReplace(transform_222)
engine.addEntity(stonePath_01_3)

const stonePath_01_4 = new Entity()
stonePath_01_4.setParent(scene)
stonePath_01_4.addComponentOrReplace(gltfShape_20)
const transform_223 = new Transform({
  position: new Vector3(43, 12, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_4.addComponentOrReplace(transform_223)
engine.addEntity(stonePath_01_4)

const stonePath_01_5 = new Entity()
stonePath_01_5.setParent(scene)
stonePath_01_5.addComponentOrReplace(gltfShape_20)
const transform_224 = new Transform({
  position: new Vector3(45, 12, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_5.addComponentOrReplace(transform_224)
engine.addEntity(stonePath_01_5)

const stonePath_01_6 = new Entity()
stonePath_01_6.setParent(scene)
stonePath_01_6.addComponentOrReplace(gltfShape_20)
const transform_225 = new Transform({
  position: new Vector3(37, 11, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_6.addComponentOrReplace(transform_225)
engine.addEntity(stonePath_01_6)

const stonePath_01_7 = new Entity()
stonePath_01_7.setParent(scene)
stonePath_01_7.addComponentOrReplace(gltfShape_20)
const transform_226 = new Transform({
  position: new Vector3(37, 10, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_7.addComponentOrReplace(transform_226)
engine.addEntity(stonePath_01_7)

const stonePath_01_8 = new Entity()
stonePath_01_8.setParent(scene)
stonePath_01_8.addComponentOrReplace(gltfShape_20)
const transform_227 = new Transform({
  position: new Vector3(37, 9, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_8.addComponentOrReplace(transform_227)
engine.addEntity(stonePath_01_8)

const stonePath_01_9 = new Entity()
stonePath_01_9.setParent(scene)
stonePath_01_9.addComponentOrReplace(gltfShape_20)
const transform_228 = new Transform({
  position: new Vector3(37, 7, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_9.addComponentOrReplace(transform_228)
engine.addEntity(stonePath_01_9)

const stonePath_01_10 = new Entity()
stonePath_01_10.setParent(scene)
stonePath_01_10.addComponentOrReplace(gltfShape_20)
const transform_229 = new Transform({
  position: new Vector3(37, 8, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_10.addComponentOrReplace(transform_229)
engine.addEntity(stonePath_01_10)

const stonePath_01_11 = new Entity()
stonePath_01_11.setParent(scene)
stonePath_01_11.addComponentOrReplace(gltfShape_20)
const transform_230 = new Transform({
  position: new Vector3(37, 6, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_11.addComponentOrReplace(transform_230)
engine.addEntity(stonePath_01_11)

const stonePath_01_12 = new Entity()
stonePath_01_12.setParent(scene)
stonePath_01_12.addComponentOrReplace(gltfShape_20)
const transform_231 = new Transform({
  position: new Vector3(37, 5, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_12.addComponentOrReplace(transform_231)
engine.addEntity(stonePath_01_12)

const stonePath_01_13 = new Entity()
stonePath_01_13.setParent(scene)
stonePath_01_13.addComponentOrReplace(gltfShape_20)
const transform_232 = new Transform({
  position: new Vector3(37, 4, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_13.addComponentOrReplace(transform_232)
engine.addEntity(stonePath_01_13)

const stonePath_01_14 = new Entity()
stonePath_01_14.setParent(scene)
stonePath_01_14.addComponentOrReplace(gltfShape_20)
const transform_233 = new Transform({
  position: new Vector3(37, 3, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_14.addComponentOrReplace(transform_233)
engine.addEntity(stonePath_01_14)

const stonePath_01_15 = new Entity()
stonePath_01_15.setParent(scene)
stonePath_01_15.addComponentOrReplace(gltfShape_20)
const transform_234 = new Transform({
  position: new Vector3(37, 2, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_15.addComponentOrReplace(transform_234)
engine.addEntity(stonePath_01_15)

const stonePath_01_16 = new Entity()
stonePath_01_16.setParent(scene)
stonePath_01_16.addComponentOrReplace(gltfShape_20)
const transform_235 = new Transform({
  position: new Vector3(37, 1, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_16.addComponentOrReplace(transform_235)
engine.addEntity(stonePath_01_16)

const stonePath_01_17 = new Entity()
stonePath_01_17.setParent(scene)
stonePath_01_17.addComponentOrReplace(gltfShape_20)
const transform_236 = new Transform({
  position: new Vector3(39, 11, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_17.addComponentOrReplace(transform_236)
engine.addEntity(stonePath_01_17)

const stonePath_01_18 = new Entity()
stonePath_01_18.setParent(scene)
stonePath_01_18.addComponentOrReplace(gltfShape_20)
const transform_237 = new Transform({
  position: new Vector3(39, 10, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_18.addComponentOrReplace(transform_237)
engine.addEntity(stonePath_01_18)

const stonePath_01_19 = new Entity()
stonePath_01_19.setParent(scene)
stonePath_01_19.addComponentOrReplace(gltfShape_20)
const transform_238 = new Transform({
  position: new Vector3(39, 9, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_19.addComponentOrReplace(transform_238)
engine.addEntity(stonePath_01_19)

const stonePath_01_20 = new Entity()
stonePath_01_20.setParent(scene)
stonePath_01_20.addComponentOrReplace(gltfShape_20)
const transform_239 = new Transform({
  position: new Vector3(39, 8, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_20.addComponentOrReplace(transform_239)
engine.addEntity(stonePath_01_20)

const stonePath_01_21 = new Entity()
stonePath_01_21.setParent(scene)
stonePath_01_21.addComponentOrReplace(gltfShape_20)
const transform_240 = new Transform({
  position: new Vector3(39, 7, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_21.addComponentOrReplace(transform_240)
engine.addEntity(stonePath_01_21)

const stonePath_01_22 = new Entity()
stonePath_01_22.setParent(scene)
stonePath_01_22.addComponentOrReplace(gltfShape_20)
const transform_241 = new Transform({
  position: new Vector3(39, 6, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_22.addComponentOrReplace(transform_241)
engine.addEntity(stonePath_01_22)

const stonePath_01_23 = new Entity()
stonePath_01_23.setParent(scene)
stonePath_01_23.addComponentOrReplace(gltfShape_20)
const transform_242 = new Transform({
  position: new Vector3(39, 5, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_23.addComponentOrReplace(transform_242)
engine.addEntity(stonePath_01_23)

const stonePath_01_24 = new Entity()
stonePath_01_24.setParent(scene)
stonePath_01_24.addComponentOrReplace(gltfShape_20)
const transform_243 = new Transform({
  position: new Vector3(39, 4, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_24.addComponentOrReplace(transform_243)
engine.addEntity(stonePath_01_24)

const stonePath_01_25 = new Entity()
stonePath_01_25.setParent(scene)
stonePath_01_25.addComponentOrReplace(gltfShape_20)
const transform_244 = new Transform({
  position: new Vector3(39, 3, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_25.addComponentOrReplace(transform_244)
engine.addEntity(stonePath_01_25)

const stonePath_01_26 = new Entity()
stonePath_01_26.setParent(scene)
stonePath_01_26.addComponentOrReplace(gltfShape_20)
const transform_245 = new Transform({
  position: new Vector3(39, 2, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_26.addComponentOrReplace(transform_245)
engine.addEntity(stonePath_01_26)

const stonePath_01_27 = new Entity()
stonePath_01_27.setParent(scene)
stonePath_01_27.addComponentOrReplace(gltfShape_20)
const transform_246 = new Transform({
  position: new Vector3(39, 1, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_27.addComponentOrReplace(transform_246)
engine.addEntity(stonePath_01_27)

const stonePath_01_28 = new Entity()
stonePath_01_28.setParent(scene)
stonePath_01_28.addComponentOrReplace(gltfShape_20)
const transform_247 = new Transform({
  position: new Vector3(41, 1, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_28.addComponentOrReplace(transform_247)
engine.addEntity(stonePath_01_28)

const stonePath_01_29 = new Entity()
stonePath_01_29.setParent(scene)
stonePath_01_29.addComponentOrReplace(gltfShape_20)
const transform_248 = new Transform({
  position: new Vector3(43, 1, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_29.addComponentOrReplace(transform_248)
engine.addEntity(stonePath_01_29)

const stonePath_01_30 = new Entity()
stonePath_01_30.setParent(scene)
stonePath_01_30.addComponentOrReplace(gltfShape_20)
const transform_249 = new Transform({
  position: new Vector3(45, 1, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_30.addComponentOrReplace(transform_249)
engine.addEntity(stonePath_01_30)

const stonePath_01_31 = new Entity()
stonePath_01_31.setParent(scene)
stonePath_01_31.addComponentOrReplace(gltfShape_20)
const transform_250 = new Transform({
  position: new Vector3(41, 11, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_31.addComponentOrReplace(transform_250)
engine.addEntity(stonePath_01_31)

const stonePath_01_32 = new Entity()
stonePath_01_32.setParent(scene)
stonePath_01_32.addComponentOrReplace(gltfShape_20)
const transform_251 = new Transform({
  position: new Vector3(41, 10, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_32.addComponentOrReplace(transform_251)
engine.addEntity(stonePath_01_32)

const stonePath_01_33 = new Entity()
stonePath_01_33.setParent(scene)
stonePath_01_33.addComponentOrReplace(gltfShape_20)
const transform_252 = new Transform({
  position: new Vector3(41, 9, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_33.addComponentOrReplace(transform_252)
engine.addEntity(stonePath_01_33)

const stonePath_01_34 = new Entity()
stonePath_01_34.setParent(scene)
stonePath_01_34.addComponentOrReplace(gltfShape_20)
const transform_253 = new Transform({
  position: new Vector3(41, 8, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_34.addComponentOrReplace(transform_253)
engine.addEntity(stonePath_01_34)

const stonePath_01_35 = new Entity()
stonePath_01_35.setParent(scene)
stonePath_01_35.addComponentOrReplace(gltfShape_20)
const transform_254 = new Transform({
  position: new Vector3(41, 7, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_35.addComponentOrReplace(transform_254)
engine.addEntity(stonePath_01_35)

const stonePath_01_36 = new Entity()
stonePath_01_36.setParent(scene)
stonePath_01_36.addComponentOrReplace(gltfShape_20)
const transform_255 = new Transform({
  position: new Vector3(41, 6, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_36.addComponentOrReplace(transform_255)
engine.addEntity(stonePath_01_36)

const stonePath_01_37 = new Entity()
stonePath_01_37.setParent(scene)
stonePath_01_37.addComponentOrReplace(gltfShape_20)
const transform_256 = new Transform({
  position: new Vector3(41, 5, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_37.addComponentOrReplace(transform_256)
engine.addEntity(stonePath_01_37)

const stonePath_01_38 = new Entity()
stonePath_01_38.setParent(scene)
stonePath_01_38.addComponentOrReplace(gltfShape_20)
const transform_257 = new Transform({
  position: new Vector3(41, 4, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_38.addComponentOrReplace(transform_257)
engine.addEntity(stonePath_01_38)

const stonePath_01_39 = new Entity()
stonePath_01_39.setParent(scene)
stonePath_01_39.addComponentOrReplace(gltfShape_20)
const transform_258 = new Transform({
  position: new Vector3(41, 3, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_39.addComponentOrReplace(transform_258)
engine.addEntity(stonePath_01_39)

const stonePath_01_40 = new Entity()
stonePath_01_40.setParent(scene)
stonePath_01_40.addComponentOrReplace(gltfShape_20)
const transform_259 = new Transform({
  position: new Vector3(41, 2, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_40.addComponentOrReplace(transform_259)
engine.addEntity(stonePath_01_40)

const stonePath_01_41 = new Entity()
stonePath_01_41.setParent(scene)
stonePath_01_41.addComponentOrReplace(gltfShape_20)
const transform_260 = new Transform({
  position: new Vector3(43, 2, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_41.addComponentOrReplace(transform_260)
engine.addEntity(stonePath_01_41)

const stonePath_01_42 = new Entity()
stonePath_01_42.setParent(scene)
stonePath_01_42.addComponentOrReplace(gltfShape_20)
const transform_261 = new Transform({
  position: new Vector3(45, 2, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_42.addComponentOrReplace(transform_261)
engine.addEntity(stonePath_01_42)

const stonePath_01_43 = new Entity()
stonePath_01_43.setParent(scene)
stonePath_01_43.addComponentOrReplace(gltfShape_20)
const transform_262 = new Transform({
  position: new Vector3(45, 3, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_43.addComponentOrReplace(transform_262)
engine.addEntity(stonePath_01_43)

const stonePath_01_44 = new Entity()
stonePath_01_44.setParent(scene)
stonePath_01_44.addComponentOrReplace(gltfShape_20)
const transform_263 = new Transform({
  position: new Vector3(45, 4, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_44.addComponentOrReplace(transform_263)
engine.addEntity(stonePath_01_44)

const stonePath_01_45 = new Entity()
stonePath_01_45.setParent(scene)
stonePath_01_45.addComponentOrReplace(gltfShape_20)
const transform_264 = new Transform({
  position: new Vector3(45, 5, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_45.addComponentOrReplace(transform_264)
engine.addEntity(stonePath_01_45)

const stonePath_01_46 = new Entity()
stonePath_01_46.setParent(scene)
stonePath_01_46.addComponentOrReplace(gltfShape_20)
const transform_265 = new Transform({
  position: new Vector3(45, 6, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_46.addComponentOrReplace(transform_265)
engine.addEntity(stonePath_01_46)

const stonePath_01_47 = new Entity()
stonePath_01_47.setParent(scene)
stonePath_01_47.addComponentOrReplace(gltfShape_20)
const transform_266 = new Transform({
  position: new Vector3(45, 7, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_47.addComponentOrReplace(transform_266)
engine.addEntity(stonePath_01_47)

const stonePath_01_48 = new Entity()
stonePath_01_48.setParent(scene)
stonePath_01_48.addComponentOrReplace(gltfShape_20)
const transform_267 = new Transform({
  position: new Vector3(45, 8, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_48.addComponentOrReplace(transform_267)
engine.addEntity(stonePath_01_48)

const stonePath_01_49 = new Entity()
stonePath_01_49.setParent(scene)
stonePath_01_49.addComponentOrReplace(gltfShape_20)
const transform_268 = new Transform({
  position: new Vector3(45, 9, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_49.addComponentOrReplace(transform_268)
engine.addEntity(stonePath_01_49)

const stonePath_01_50 = new Entity()
stonePath_01_50.setParent(scene)
stonePath_01_50.addComponentOrReplace(gltfShape_20)
const transform_269 = new Transform({
  position: new Vector3(45, 10, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_50.addComponentOrReplace(transform_269)
engine.addEntity(stonePath_01_50)

const stonePath_01_51 = new Entity()
stonePath_01_51.setParent(scene)
stonePath_01_51.addComponentOrReplace(gltfShape_20)
const transform_270 = new Transform({
  position: new Vector3(45, 11, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_51.addComponentOrReplace(transform_270)
engine.addEntity(stonePath_01_51)

const stonePath_01_52 = new Entity()
stonePath_01_52.setParent(scene)
stonePath_01_52.addComponentOrReplace(gltfShape_20)
const transform_271 = new Transform({
  position: new Vector3(43, 3, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_52.addComponentOrReplace(transform_271)
engine.addEntity(stonePath_01_52)

const stonePath_01_53 = new Entity()
stonePath_01_53.setParent(scene)
stonePath_01_53.addComponentOrReplace(gltfShape_20)
const transform_272 = new Transform({
  position: new Vector3(43, 4, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_53.addComponentOrReplace(transform_272)
engine.addEntity(stonePath_01_53)

const stonePath_01_54 = new Entity()
stonePath_01_54.setParent(scene)
stonePath_01_54.addComponentOrReplace(gltfShape_20)
const transform_273 = new Transform({
  position: new Vector3(43, 5, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_54.addComponentOrReplace(transform_273)
engine.addEntity(stonePath_01_54)

const stonePath_01_55 = new Entity()
stonePath_01_55.setParent(scene)
stonePath_01_55.addComponentOrReplace(gltfShape_20)
const transform_274 = new Transform({
  position: new Vector3(43, 6, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_55.addComponentOrReplace(transform_274)
engine.addEntity(stonePath_01_55)

const stonePath_01_56 = new Entity()
stonePath_01_56.setParent(scene)
stonePath_01_56.addComponentOrReplace(gltfShape_20)
const transform_275 = new Transform({
  position: new Vector3(43, 7, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_56.addComponentOrReplace(transform_275)
engine.addEntity(stonePath_01_56)

const stonePath_01_57 = new Entity()
stonePath_01_57.setParent(scene)
stonePath_01_57.addComponentOrReplace(gltfShape_20)
const transform_276 = new Transform({
  position: new Vector3(43, 8, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_57.addComponentOrReplace(transform_276)
engine.addEntity(stonePath_01_57)

const stonePath_01_58 = new Entity()
stonePath_01_58.setParent(scene)
stonePath_01_58.addComponentOrReplace(gltfShape_20)
const transform_277 = new Transform({
  position: new Vector3(43, 9, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_58.addComponentOrReplace(transform_277)
engine.addEntity(stonePath_01_58)

const stonePath_01_59 = new Entity()
stonePath_01_59.setParent(scene)
stonePath_01_59.addComponentOrReplace(gltfShape_20)
const transform_278 = new Transform({
  position: new Vector3(43, 10, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_59.addComponentOrReplace(transform_278)
engine.addEntity(stonePath_01_59)

const stonePath_01_60 = new Entity()
stonePath_01_60.setParent(scene)
stonePath_01_60.addComponentOrReplace(gltfShape_20)
const transform_279 = new Transform({
  position: new Vector3(43, 11, 5.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_60.addComponentOrReplace(transform_279)
engine.addEntity(stonePath_01_60)

const rockPillars_01 = new Entity()
rockPillars_01.setParent(scene)
const gltfShape_21 = new GLTFShape('models/RockPillars_01/RockPillars_01.glb')
rockPillars_01.addComponentOrReplace(gltfShape_21)
const transform_280 = new Transform({
  position: new Vector3(46, 0, 7),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
rockPillars_01.addComponentOrReplace(transform_280)
engine.addEntity(rockPillars_01)

const rockPillars_01_2 = new Entity()
rockPillars_01_2.setParent(scene)
rockPillars_01_2.addComponentOrReplace(gltfShape_21)
const transform_281 = new Transform({
  position: new Vector3(46, 2.5, 7),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
rockPillars_01_2.addComponentOrReplace(transform_281)
engine.addEntity(rockPillars_01_2)

const rockPillars_01_3 = new Entity()
rockPillars_01_3.setParent(scene)
rockPillars_01_3.addComponentOrReplace(gltfShape_21)
const transform_282 = new Transform({
  position: new Vector3(46, 5, 7),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
rockPillars_01_3.addComponentOrReplace(transform_282)
engine.addEntity(rockPillars_01_3)

const rockPillars_01_4 = new Entity()
rockPillars_01_4.setParent(scene)
rockPillars_01_4.addComponentOrReplace(gltfShape_21)
const transform_283 = new Transform({
  position: new Vector3(46, 7.5, 7),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
rockPillars_01_4.addComponentOrReplace(transform_283)
engine.addEntity(rockPillars_01_4)

const rockPillars_01_5 = new Entity()
rockPillars_01_5.setParent(scene)
rockPillars_01_5.addComponentOrReplace(gltfShape_21)
const transform_284 = new Transform({
  position: new Vector3(46, 7.5, 22.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
rockPillars_01_5.addComponentOrReplace(transform_284)
engine.addEntity(rockPillars_01_5)

const rockPillars_01_6 = new Entity()
rockPillars_01_6.setParent(scene)
rockPillars_01_6.addComponentOrReplace(gltfShape_21)
const transform_285 = new Transform({
  position: new Vector3(46, 5, 22.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
rockPillars_01_6.addComponentOrReplace(transform_285)
engine.addEntity(rockPillars_01_6)

const rockPillars_01_7 = new Entity()
rockPillars_01_7.setParent(scene)
rockPillars_01_7.addComponentOrReplace(gltfShape_21)
const transform_286 = new Transform({
  position: new Vector3(46, 2.5, 22.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
rockPillars_01_7.addComponentOrReplace(transform_286)
engine.addEntity(rockPillars_01_7)

const rockPillars_01_8 = new Entity()
rockPillars_01_8.setParent(scene)
rockPillars_01_8.addComponentOrReplace(gltfShape_21)
const transform_287 = new Transform({
  position: new Vector3(46, 0, 22.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
rockPillars_01_8.addComponentOrReplace(transform_287)
engine.addEntity(rockPillars_01_8)

const stonePath_01_61 = new Entity()
stonePath_01_61.setParent(scene)
stonePath_01_61.addComponentOrReplace(gltfShape_20)
const transform_288 = new Transform({
  position: new Vector3(45.5, 12, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_61.addComponentOrReplace(transform_288)
engine.addEntity(stonePath_01_61)

const stonePath_01_62 = new Entity()
stonePath_01_62.setParent(scene)
stonePath_01_62.addComponentOrReplace(gltfShape_20)
const transform_289 = new Transform({
  position: new Vector3(45.5, 12, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_62.addComponentOrReplace(transform_289)
engine.addEntity(stonePath_01_62)

const stonePath_01_63 = new Entity()
stonePath_01_63.setParent(scene)
stonePath_01_63.addComponentOrReplace(gltfShape_20)
const transform_290 = new Transform({
  position: new Vector3(45.5, 11, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_63.addComponentOrReplace(transform_290)
engine.addEntity(stonePath_01_63)

const stonePath_01_64 = new Entity()
stonePath_01_64.setParent(scene)
stonePath_01_64.addComponentOrReplace(gltfShape_20)
const transform_291 = new Transform({
  position: new Vector3(45.5, 10, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_64.addComponentOrReplace(transform_291)
engine.addEntity(stonePath_01_64)

const stonePath_01_65 = new Entity()
stonePath_01_65.setParent(scene)
stonePath_01_65.addComponentOrReplace(gltfShape_20)
const transform_292 = new Transform({
  position: new Vector3(45.5, 9, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_65.addComponentOrReplace(transform_292)
engine.addEntity(stonePath_01_65)

const stonePath_01_66 = new Entity()
stonePath_01_66.setParent(scene)
stonePath_01_66.addComponentOrReplace(gltfShape_20)
const transform_293 = new Transform({
  position: new Vector3(45.5, 8, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_66.addComponentOrReplace(transform_293)
engine.addEntity(stonePath_01_66)

const stonePath_01_67 = new Entity()
stonePath_01_67.setParent(scene)
stonePath_01_67.addComponentOrReplace(gltfShape_20)
const transform_294 = new Transform({
  position: new Vector3(45.5, 7, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_67.addComponentOrReplace(transform_294)
engine.addEntity(stonePath_01_67)

const stonePath_01_68 = new Entity()
stonePath_01_68.setParent(scene)
stonePath_01_68.addComponentOrReplace(gltfShape_20)
const transform_295 = new Transform({
  position: new Vector3(45.5, 6, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_68.addComponentOrReplace(transform_295)
engine.addEntity(stonePath_01_68)

const stonePath_01_69 = new Entity()
stonePath_01_69.setParent(scene)
stonePath_01_69.addComponentOrReplace(gltfShape_20)
const transform_296 = new Transform({
  position: new Vector3(45.5, 5, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_69.addComponentOrReplace(transform_296)
engine.addEntity(stonePath_01_69)

const stonePath_01_70 = new Entity()
stonePath_01_70.setParent(scene)
stonePath_01_70.addComponentOrReplace(gltfShape_20)
const transform_297 = new Transform({
  position: new Vector3(45.5, 4, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_70.addComponentOrReplace(transform_297)
engine.addEntity(stonePath_01_70)

const stonePath_01_71 = new Entity()
stonePath_01_71.setParent(scene)
stonePath_01_71.addComponentOrReplace(gltfShape_20)
const transform_298 = new Transform({
  position: new Vector3(45.5, 3, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_71.addComponentOrReplace(transform_298)
engine.addEntity(stonePath_01_71)

const stonePath_01_72 = new Entity()
stonePath_01_72.setParent(scene)
stonePath_01_72.addComponentOrReplace(gltfShape_20)
const transform_299 = new Transform({
  position: new Vector3(45.5, 2, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_72.addComponentOrReplace(transform_299)
engine.addEntity(stonePath_01_72)

const stonePath_01_73 = new Entity()
stonePath_01_73.setParent(scene)
stonePath_01_73.addComponentOrReplace(gltfShape_20)
const transform_300 = new Transform({
  position: new Vector3(45.5, 1, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_73.addComponentOrReplace(transform_300)
engine.addEntity(stonePath_01_73)

const stonePath_01_74 = new Entity()
stonePath_01_74.setParent(scene)
stonePath_01_74.addComponentOrReplace(gltfShape_20)
const transform_301 = new Transform({
  position: new Vector3(45.5, 1, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_74.addComponentOrReplace(transform_301)
engine.addEntity(stonePath_01_74)

const stonePath_01_75 = new Entity()
stonePath_01_75.setParent(scene)
stonePath_01_75.addComponentOrReplace(gltfShape_20)
const transform_302 = new Transform({
  position: new Vector3(43.5, 1, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_75.addComponentOrReplace(transform_302)
engine.addEntity(stonePath_01_75)

const stonePath_01_76 = new Entity()
stonePath_01_76.setParent(scene)
stonePath_01_76.addComponentOrReplace(gltfShape_20)
const transform_303 = new Transform({
  position: new Vector3(41.5, 1, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_76.addComponentOrReplace(transform_303)
engine.addEntity(stonePath_01_76)

const stonePath_01_77 = new Entity()
stonePath_01_77.setParent(scene)
stonePath_01_77.addComponentOrReplace(gltfShape_20)
const transform_304 = new Transform({
  position: new Vector3(39.5, 1, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_77.addComponentOrReplace(transform_304)
engine.addEntity(stonePath_01_77)

const stonePath_01_78 = new Entity()
stonePath_01_78.setParent(scene)
stonePath_01_78.addComponentOrReplace(gltfShape_20)
const transform_305 = new Transform({
  position: new Vector3(37.5, 1, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_78.addComponentOrReplace(transform_305)
engine.addEntity(stonePath_01_78)

const stonePath_01_79 = new Entity()
stonePath_01_79.setParent(scene)
stonePath_01_79.addComponentOrReplace(gltfShape_20)
const transform_306 = new Transform({
  position: new Vector3(36, 1, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_79.addComponentOrReplace(transform_306)
engine.addEntity(stonePath_01_79)

const stonePath_01_80 = new Entity()
stonePath_01_80.setParent(scene)
stonePath_01_80.addComponentOrReplace(gltfShape_20)
const transform_307 = new Transform({
  position: new Vector3(36, 2, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_80.addComponentOrReplace(transform_307)
engine.addEntity(stonePath_01_80)

const stonePath_01_81 = new Entity()
stonePath_01_81.setParent(scene)
stonePath_01_81.addComponentOrReplace(gltfShape_20)
const transform_308 = new Transform({
  position: new Vector3(36, 3, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_81.addComponentOrReplace(transform_308)
engine.addEntity(stonePath_01_81)

const stonePath_01_82 = new Entity()
stonePath_01_82.setParent(scene)
stonePath_01_82.addComponentOrReplace(gltfShape_20)
const transform_309 = new Transform({
  position: new Vector3(36, 4, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_82.addComponentOrReplace(transform_309)
engine.addEntity(stonePath_01_82)

const stonePath_01_83 = new Entity()
stonePath_01_83.setParent(scene)
stonePath_01_83.addComponentOrReplace(gltfShape_20)
const transform_310 = new Transform({
  position: new Vector3(36, 5, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_83.addComponentOrReplace(transform_310)
engine.addEntity(stonePath_01_83)

const stonePath_01_84 = new Entity()
stonePath_01_84.setParent(scene)
stonePath_01_84.addComponentOrReplace(gltfShape_20)
const transform_311 = new Transform({
  position: new Vector3(36, 6, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_84.addComponentOrReplace(transform_311)
engine.addEntity(stonePath_01_84)

const stonePath_01_85 = new Entity()
stonePath_01_85.setParent(scene)
stonePath_01_85.addComponentOrReplace(gltfShape_20)
const transform_312 = new Transform({
  position: new Vector3(36, 7, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_85.addComponentOrReplace(transform_312)
engine.addEntity(stonePath_01_85)

const stonePath_01_86 = new Entity()
stonePath_01_86.setParent(scene)
stonePath_01_86.addComponentOrReplace(gltfShape_20)
const transform_313 = new Transform({
  position: new Vector3(36, 8, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_86.addComponentOrReplace(transform_313)
engine.addEntity(stonePath_01_86)

const stonePath_01_87 = new Entity()
stonePath_01_87.setParent(scene)
stonePath_01_87.addComponentOrReplace(gltfShape_20)
const transform_314 = new Transform({
  position: new Vector3(36, 9, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_87.addComponentOrReplace(transform_314)
engine.addEntity(stonePath_01_87)

const stonePath_01_88 = new Entity()
stonePath_01_88.setParent(scene)
stonePath_01_88.addComponentOrReplace(gltfShape_20)
const transform_315 = new Transform({
  position: new Vector3(36, 10, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_88.addComponentOrReplace(transform_315)
engine.addEntity(stonePath_01_88)

const stonePath_01_89 = new Entity()
stonePath_01_89.setParent(scene)
stonePath_01_89.addComponentOrReplace(gltfShape_20)
const transform_316 = new Transform({
  position: new Vector3(36, 11, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_89.addComponentOrReplace(transform_316)
engine.addEntity(stonePath_01_89)

const stonePath_01_90 = new Entity()
stonePath_01_90.setParent(scene)
stonePath_01_90.addComponentOrReplace(gltfShape_20)
const transform_317 = new Transform({
  position: new Vector3(36, 12, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_90.addComponentOrReplace(transform_317)
engine.addEntity(stonePath_01_90)

const stonePath_01_91 = new Entity()
stonePath_01_91.setParent(scene)
stonePath_01_91.addComponentOrReplace(gltfShape_20)
const transform_318 = new Transform({
  position: new Vector3(37.5, 2, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_91.addComponentOrReplace(transform_318)
engine.addEntity(stonePath_01_91)

const stonePath_01_92 = new Entity()
stonePath_01_92.setParent(scene)
stonePath_01_92.addComponentOrReplace(gltfShape_20)
const transform_319 = new Transform({
  position: new Vector3(37.5, 3, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_92.addComponentOrReplace(transform_319)
engine.addEntity(stonePath_01_92)

const stonePath_01_93 = new Entity()
stonePath_01_93.setParent(scene)
stonePath_01_93.addComponentOrReplace(gltfShape_20)
const transform_320 = new Transform({
  position: new Vector3(37.5, 4, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_93.addComponentOrReplace(transform_320)
engine.addEntity(stonePath_01_93)

const stonePath_01_94 = new Entity()
stonePath_01_94.setParent(scene)
stonePath_01_94.addComponentOrReplace(gltfShape_20)
const transform_321 = new Transform({
  position: new Vector3(37.5, 5, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_94.addComponentOrReplace(transform_321)
engine.addEntity(stonePath_01_94)

const stonePath_01_95 = new Entity()
stonePath_01_95.setParent(scene)
stonePath_01_95.addComponentOrReplace(gltfShape_20)
const transform_322 = new Transform({
  position: new Vector3(37.5, 6, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_95.addComponentOrReplace(transform_322)
engine.addEntity(stonePath_01_95)

const stonePath_01_96 = new Entity()
stonePath_01_96.setParent(scene)
stonePath_01_96.addComponentOrReplace(gltfShape_20)
const transform_323 = new Transform({
  position: new Vector3(37.5, 7, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_96.addComponentOrReplace(transform_323)
engine.addEntity(stonePath_01_96)

const stonePath_01_97 = new Entity()
stonePath_01_97.setParent(scene)
stonePath_01_97.addComponentOrReplace(gltfShape_20)
const transform_324 = new Transform({
  position: new Vector3(39.5, 2, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_97.addComponentOrReplace(transform_324)
engine.addEntity(stonePath_01_97)

const stonePath_01_98 = new Entity()
stonePath_01_98.setParent(scene)
stonePath_01_98.addComponentOrReplace(gltfShape_20)
const transform_325 = new Transform({
  position: new Vector3(39.5, 3, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_98.addComponentOrReplace(transform_325)
engine.addEntity(stonePath_01_98)

const stonePath_01_99 = new Entity()
stonePath_01_99.setParent(scene)
stonePath_01_99.addComponentOrReplace(gltfShape_20)
const transform_326 = new Transform({
  position: new Vector3(39.5, 4, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_99.addComponentOrReplace(transform_326)
engine.addEntity(stonePath_01_99)

const stonePath_01_100 = new Entity()
stonePath_01_100.setParent(scene)
stonePath_01_100.addComponentOrReplace(gltfShape_20)
const transform_327 = new Transform({
  position: new Vector3(39.5, 5, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_100.addComponentOrReplace(transform_327)
engine.addEntity(stonePath_01_100)

const stonePath_01_101 = new Entity()
stonePath_01_101.setParent(scene)
stonePath_01_101.addComponentOrReplace(gltfShape_20)
const transform_328 = new Transform({
  position: new Vector3(39.5, 6, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_101.addComponentOrReplace(transform_328)
engine.addEntity(stonePath_01_101)

const stonePath_01_102 = new Entity()
stonePath_01_102.setParent(scene)
stonePath_01_102.addComponentOrReplace(gltfShape_20)
const transform_329 = new Transform({
  position: new Vector3(39.5, 7, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_102.addComponentOrReplace(transform_329)
engine.addEntity(stonePath_01_102)

const stonePath_01_103 = new Entity()
stonePath_01_103.setParent(scene)
stonePath_01_103.addComponentOrReplace(gltfShape_20)
const transform_330 = new Transform({
  position: new Vector3(37.5, 8, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_103.addComponentOrReplace(transform_330)
engine.addEntity(stonePath_01_103)

const stonePath_01_104 = new Entity()
stonePath_01_104.setParent(scene)
stonePath_01_104.addComponentOrReplace(gltfShape_20)
const transform_331 = new Transform({
  position: new Vector3(37.5, 9, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_104.addComponentOrReplace(transform_331)
engine.addEntity(stonePath_01_104)

const stonePath_01_105 = new Entity()
stonePath_01_105.setParent(scene)
stonePath_01_105.addComponentOrReplace(gltfShape_20)
const transform_332 = new Transform({
  position: new Vector3(37.5, 10, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_105.addComponentOrReplace(transform_332)
engine.addEntity(stonePath_01_105)

const stonePath_01_106 = new Entity()
stonePath_01_106.setParent(scene)
stonePath_01_106.addComponentOrReplace(gltfShape_20)
const transform_333 = new Transform({
  position: new Vector3(37.5, 11, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_106.addComponentOrReplace(transform_333)
engine.addEntity(stonePath_01_106)

const stonePath_01_107 = new Entity()
stonePath_01_107.setParent(scene)
stonePath_01_107.addComponentOrReplace(gltfShape_20)
const transform_334 = new Transform({
  position: new Vector3(37.5, 12, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_107.addComponentOrReplace(transform_334)
engine.addEntity(stonePath_01_107)

const stonePath_01_108 = new Entity()
stonePath_01_108.setParent(scene)
stonePath_01_108.addComponentOrReplace(gltfShape_20)
const transform_335 = new Transform({
  position: new Vector3(39.5, 8, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_108.addComponentOrReplace(transform_335)
engine.addEntity(stonePath_01_108)

const stonePath_01_109 = new Entity()
stonePath_01_109.setParent(scene)
stonePath_01_109.addComponentOrReplace(gltfShape_20)
const transform_336 = new Transform({
  position: new Vector3(39.5, 9, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_109.addComponentOrReplace(transform_336)
engine.addEntity(stonePath_01_109)

const stonePath_01_110 = new Entity()
stonePath_01_110.setParent(scene)
stonePath_01_110.addComponentOrReplace(gltfShape_20)
const transform_337 = new Transform({
  position: new Vector3(39.5, 10, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_110.addComponentOrReplace(transform_337)
engine.addEntity(stonePath_01_110)

const stonePath_01_111 = new Entity()
stonePath_01_111.setParent(scene)
stonePath_01_111.addComponentOrReplace(gltfShape_20)
const transform_338 = new Transform({
  position: new Vector3(39.5, 11, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_111.addComponentOrReplace(transform_338)
engine.addEntity(stonePath_01_111)

const stonePath_01_112 = new Entity()
stonePath_01_112.setParent(scene)
stonePath_01_112.addComponentOrReplace(gltfShape_20)
const transform_339 = new Transform({
  position: new Vector3(39.5, 12, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_112.addComponentOrReplace(transform_339)
engine.addEntity(stonePath_01_112)

const stonePath_01_113 = new Entity()
stonePath_01_113.setParent(scene)
stonePath_01_113.addComponentOrReplace(gltfShape_20)
const transform_340 = new Transform({
  position: new Vector3(41.5, 2, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_113.addComponentOrReplace(transform_340)
engine.addEntity(stonePath_01_113)

const stonePath_01_114 = new Entity()
stonePath_01_114.setParent(scene)
stonePath_01_114.addComponentOrReplace(gltfShape_20)
const transform_341 = new Transform({
  position: new Vector3(41.5, 3, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_114.addComponentOrReplace(transform_341)
engine.addEntity(stonePath_01_114)

const stonePath_01_115 = new Entity()
stonePath_01_115.setParent(scene)
stonePath_01_115.addComponentOrReplace(gltfShape_20)
const transform_342 = new Transform({
  position: new Vector3(41.5, 4, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_115.addComponentOrReplace(transform_342)
engine.addEntity(stonePath_01_115)

const stonePath_01_116 = new Entity()
stonePath_01_116.setParent(scene)
stonePath_01_116.addComponentOrReplace(gltfShape_20)
const transform_343 = new Transform({
  position: new Vector3(41.5, 5, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_116.addComponentOrReplace(transform_343)
engine.addEntity(stonePath_01_116)

const stonePath_01_117 = new Entity()
stonePath_01_117.setParent(scene)
stonePath_01_117.addComponentOrReplace(gltfShape_20)
const transform_344 = new Transform({
  position: new Vector3(41.5, 6, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_117.addComponentOrReplace(transform_344)
engine.addEntity(stonePath_01_117)

const stonePath_01_118 = new Entity()
stonePath_01_118.setParent(scene)
stonePath_01_118.addComponentOrReplace(gltfShape_20)
const transform_345 = new Transform({
  position: new Vector3(41.5, 7, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_118.addComponentOrReplace(transform_345)
engine.addEntity(stonePath_01_118)

const stonePath_01_119 = new Entity()
stonePath_01_119.setParent(scene)
stonePath_01_119.addComponentOrReplace(gltfShape_20)
const transform_346 = new Transform({
  position: new Vector3(41.5, 8, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_119.addComponentOrReplace(transform_346)
engine.addEntity(stonePath_01_119)

const stonePath_01_120 = new Entity()
stonePath_01_120.setParent(scene)
stonePath_01_120.addComponentOrReplace(gltfShape_20)
const transform_347 = new Transform({
  position: new Vector3(41.5, 9, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_120.addComponentOrReplace(transform_347)
engine.addEntity(stonePath_01_120)

const stonePath_01_121 = new Entity()
stonePath_01_121.setParent(scene)
stonePath_01_121.addComponentOrReplace(gltfShape_20)
const transform_348 = new Transform({
  position: new Vector3(43.5, 2, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_121.addComponentOrReplace(transform_348)
engine.addEntity(stonePath_01_121)

const stonePath_01_122 = new Entity()
stonePath_01_122.setParent(scene)
stonePath_01_122.addComponentOrReplace(gltfShape_20)
const transform_349 = new Transform({
  position: new Vector3(43.5, 3, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_122.addComponentOrReplace(transform_349)
engine.addEntity(stonePath_01_122)

const stonePath_01_123 = new Entity()
stonePath_01_123.setParent(scene)
stonePath_01_123.addComponentOrReplace(gltfShape_20)
const transform_350 = new Transform({
  position: new Vector3(43.5, 4, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_123.addComponentOrReplace(transform_350)
engine.addEntity(stonePath_01_123)

const stonePath_01_124 = new Entity()
stonePath_01_124.setParent(scene)
stonePath_01_124.addComponentOrReplace(gltfShape_20)
const transform_351 = new Transform({
  position: new Vector3(43.5, 5, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_124.addComponentOrReplace(transform_351)
engine.addEntity(stonePath_01_124)

const stonePath_01_125 = new Entity()
stonePath_01_125.setParent(scene)
stonePath_01_125.addComponentOrReplace(gltfShape_20)
const transform_352 = new Transform({
  position: new Vector3(43.5, 6, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_125.addComponentOrReplace(transform_352)
engine.addEntity(stonePath_01_125)

const stonePath_01_126 = new Entity()
stonePath_01_126.setParent(scene)
stonePath_01_126.addComponentOrReplace(gltfShape_20)
const transform_353 = new Transform({
  position: new Vector3(43.5, 7, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_126.addComponentOrReplace(transform_353)
engine.addEntity(stonePath_01_126)

const stonePath_01_127 = new Entity()
stonePath_01_127.setParent(scene)
stonePath_01_127.addComponentOrReplace(gltfShape_20)
const transform_354 = new Transform({
  position: new Vector3(43.5, 8, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_127.addComponentOrReplace(transform_354)
engine.addEntity(stonePath_01_127)

const stonePath_01_128 = new Entity()
stonePath_01_128.setParent(scene)
stonePath_01_128.addComponentOrReplace(gltfShape_20)
const transform_355 = new Transform({
  position: new Vector3(43.5, 9, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_128.addComponentOrReplace(transform_355)
engine.addEntity(stonePath_01_128)

const stonePath_01_129 = new Entity()
stonePath_01_129.setParent(scene)
stonePath_01_129.addComponentOrReplace(gltfShape_20)
const transform_356 = new Transform({
  position: new Vector3(41.5, 10, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_129.addComponentOrReplace(transform_356)
engine.addEntity(stonePath_01_129)

const stonePath_01_130 = new Entity()
stonePath_01_130.setParent(scene)
stonePath_01_130.addComponentOrReplace(gltfShape_20)
const transform_357 = new Transform({
  position: new Vector3(41.5, 11, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_130.addComponentOrReplace(transform_357)
engine.addEntity(stonePath_01_130)

const stonePath_01_131 = new Entity()
stonePath_01_131.setParent(scene)
stonePath_01_131.addComponentOrReplace(gltfShape_20)
const transform_358 = new Transform({
  position: new Vector3(41.5, 12, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_131.addComponentOrReplace(transform_358)
engine.addEntity(stonePath_01_131)

const stonePath_01_132 = new Entity()
stonePath_01_132.setParent(scene)
stonePath_01_132.addComponentOrReplace(gltfShape_20)
const transform_359 = new Transform({
  position: new Vector3(43.5, 10, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_132.addComponentOrReplace(transform_359)
engine.addEntity(stonePath_01_132)

const stonePath_01_133 = new Entity()
stonePath_01_133.setParent(scene)
stonePath_01_133.addComponentOrReplace(gltfShape_20)
const transform_360 = new Transform({
  position: new Vector3(43.5, 11, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_133.addComponentOrReplace(transform_360)
engine.addEntity(stonePath_01_133)

const stonePath_01_134 = new Entity()
stonePath_01_134.setParent(scene)
stonePath_01_134.addComponentOrReplace(gltfShape_20)
const transform_361 = new Transform({
  position: new Vector3(43.5, 12, 23.5),
  rotation: new Quaternion(0.7071067811865476, 7.731666819022492e-17, 9.421063277650482e-17, -0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
stonePath_01_134.addComponentOrReplace(transform_361)
engine.addEntity(stonePath_01_134)

const rockPillars_01_9 = new Entity()
rockPillars_01_9.setParent(scene)
rockPillars_01_9.addComponentOrReplace(gltfShape_21)
const transform_362 = new Transform({
  position: new Vector3(34, 0, 22),
  rotation: new Quaternion(0, -0.7730104533627373, 0, 0.6343932841636459),
  scale: new Vector3(1, 1, 1)
})
rockPillars_01_9.addComponentOrReplace(transform_362)
engine.addEntity(rockPillars_01_9)

const rockPillars_01_10 = new Entity()
rockPillars_01_10.setParent(scene)
rockPillars_01_10.addComponentOrReplace(gltfShape_21)
const transform_363 = new Transform({
  position: new Vector3(34, 2.5, 22),
  rotation: new Quaternion(0, -0.7730104533627373, 0, 0.6343932841636459),
  scale: new Vector3(1, 1, 1)
})
rockPillars_01_10.addComponentOrReplace(transform_363)
engine.addEntity(rockPillars_01_10)

const rockPillars_01_11 = new Entity()
rockPillars_01_11.setParent(scene)
rockPillars_01_11.addComponentOrReplace(gltfShape_21)
const transform_364 = new Transform({
  position: new Vector3(34, 5, 22),
  rotation: new Quaternion(0, -0.7730104533627373, 0, 0.6343932841636459),
  scale: new Vector3(1, 1, 1)
})
rockPillars_01_11.addComponentOrReplace(transform_364)
engine.addEntity(rockPillars_01_11)

const rockPillars_01_12 = new Entity()
rockPillars_01_12.setParent(scene)
rockPillars_01_12.addComponentOrReplace(gltfShape_21)
const transform_365 = new Transform({
  position: new Vector3(34, 7, 22),
  rotation: new Quaternion(0, -0.7730104533627373, 0, 0.6343932841636459),
  scale: new Vector3(1, 1, 1)
})
rockPillars_01_12.addComponentOrReplace(transform_365)
engine.addEntity(rockPillars_01_12)

const rockPillars_01_13 = new Entity()
rockPillars_01_13.setParent(scene)
rockPillars_01_13.addComponentOrReplace(gltfShape_21)
const transform_366 = new Transform({
  position: new Vector3(34, 9, 22),
  rotation: new Quaternion(0, -0.7730104533627373, 0, 0.6343932841636459),
  scale: new Vector3(1, 1, 1)
})
rockPillars_01_13.addComponentOrReplace(transform_366)
engine.addEntity(rockPillars_01_13)

const rockPillars_01_14 = new Entity()
rockPillars_01_14.setParent(scene)
rockPillars_01_14.addComponentOrReplace(gltfShape_21)
const transform_367 = new Transform({
  position: new Vector3(34, 0, 6),
  rotation: new Quaternion(0, 0.995184726672197, 0, -0.0980171403295605),
  scale: new Vector3(1, 1, 1)
})
rockPillars_01_14.addComponentOrReplace(transform_367)
engine.addEntity(rockPillars_01_14)

const rockPillars_01_15 = new Entity()
rockPillars_01_15.setParent(scene)
rockPillars_01_15.addComponentOrReplace(gltfShape_21)
const transform_368 = new Transform({
  position: new Vector3(34, 2.5, 6),
  rotation: new Quaternion(0, 0.995184726672197, 0, -0.0980171403295605),
  scale: new Vector3(1, 1, 1)
})
rockPillars_01_15.addComponentOrReplace(transform_368)
engine.addEntity(rockPillars_01_15)

const rockPillars_01_16 = new Entity()
rockPillars_01_16.setParent(scene)
rockPillars_01_16.addComponentOrReplace(gltfShape_21)
const transform_369 = new Transform({
  position: new Vector3(34, 4.5, 6),
  rotation: new Quaternion(0, 0.995184726672197, 0, -0.0980171403295605),
  scale: new Vector3(1, 1, 1)
})
rockPillars_01_16.addComponentOrReplace(transform_369)
engine.addEntity(rockPillars_01_16)

const rockPillars_01_17 = new Entity()
rockPillars_01_17.setParent(scene)
rockPillars_01_17.addComponentOrReplace(gltfShape_21)
const transform_370 = new Transform({
  position: new Vector3(34, 6.5, 6),
  rotation: new Quaternion(0, 0.995184726672197, 0, -0.0980171403295605),
  scale: new Vector3(1, 1, 1)
})
rockPillars_01_17.addComponentOrReplace(transform_370)
engine.addEntity(rockPillars_01_17)

const rockPillars_01_18 = new Entity()
rockPillars_01_18.setParent(scene)
rockPillars_01_18.addComponentOrReplace(gltfShape_21)
const transform_371 = new Transform({
  position: new Vector3(34, 8.5, 6),
  rotation: new Quaternion(0, 0.995184726672197, 0, -0.0980171403295605),
  scale: new Vector3(1, 1, 1)
})
rockPillars_01_18.addComponentOrReplace(transform_371)
engine.addEntity(rockPillars_01_18)

const treeSycamore_01 = new Entity()
treeSycamore_01.setParent(scene)
const gltfShape_22 = new GLTFShape('models/TreeSycamore_01/TreeSycamore_01.glb')
treeSycamore_01.addComponentOrReplace(gltfShape_22)
const transform_372 = new Transform({
  position: new Vector3(37.5, 0, 20.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
treeSycamore_01.addComponentOrReplace(transform_372)
engine.addEntity(treeSycamore_01)

const treeSycamore_01_2 = new Entity()
treeSycamore_01_2.setParent(scene)
treeSycamore_01_2.addComponentOrReplace(gltfShape_22)
const transform_373 = new Transform({
  position: new Vector3(44.5, 0, 8.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
treeSycamore_01_2.addComponentOrReplace(transform_373)
engine.addEntity(treeSycamore_01_2)

const treeSycamore_01_3 = new Entity()
treeSycamore_01_3.setParent(scene)
treeSycamore_01_3.addComponentOrReplace(gltfShape_22)
const transform_374 = new Transform({
  position: new Vector3(33.5, 0, 9),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
treeSycamore_01_3.addComponentOrReplace(transform_374)
engine.addEntity(treeSycamore_01_3)

const treeSycamore_01_4 = new Entity()
treeSycamore_01_4.setParent(scene)
treeSycamore_01_4.addComponentOrReplace(gltfShape_22)
const transform_375 = new Transform({
  position: new Vector3(44.5, 0, 20),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
treeSycamore_01_4.addComponentOrReplace(transform_375)
engine.addEntity(treeSycamore_01_4)

const pond_02 = new Entity()
pond_02.setParent(scene)
const gltfShape_23 = new GLTFShape('models/Pond_02/Pond_02.glb')
pond_02.addComponentOrReplace(gltfShape_23)
const transform_376 = new Transform({
  position: new Vector3(40.5, 0, 15),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
pond_02.addComponentOrReplace(transform_376)
engine.addEntity(pond_02)

const chineseStatueLion_01 = new Entity()
chineseStatueLion_01.setParent(scene)
const gltfShape_24 = new GLTFShape('models/ChineseStatueLion_01/ChineseStatueLion_01.glb')
chineseStatueLion_01.addComponentOrReplace(gltfShape_24)
const transform_377 = new Transform({
  position: new Vector3(14.5, 0, 25.5),
  rotation: new Quaternion(0, 1.8041124150158794e-16, 0, 1.0000000000000002),
  scale: new Vector3(1, 1, 1)
})
chineseStatueLion_01.addComponentOrReplace(transform_377)
engine.addEntity(chineseStatueLion_01)

const chineseStatueLion_01_2 = new Entity()
chineseStatueLion_01_2.setParent(scene)
chineseStatueLion_01_2.addComponentOrReplace(gltfShape_24)
const transform_378 = new Transform({
  position: new Vector3(18.5, 0, 25),
  rotation: new Quaternion(0, 2.7755575615628914e-17, 0, 1.0000000000000002),
  scale: new Vector3(1, 1, 1)
})
chineseStatueLion_01_2.addComponentOrReplace(transform_378)
engine.addEntity(chineseStatueLion_01_2)

const chineseGate_03 = new Entity()
chineseGate_03.setParent(scene)
const gltfShape_25 = new GLTFShape('models/ChineseGate_03/ChineseGate_03.glb')
chineseGate_03.addComponentOrReplace(gltfShape_25)
const transform_379 = new Transform({
  position: new Vector3(30.5, 0, 15.5),
  rotation: new Quaternion(0, -0.7071067811865476, 0, 0.7071067811865476),
  scale: new Vector3(1, 1, 1)
})
chineseGate_03.addComponentOrReplace(transform_379)
engine.addEntity(chineseGate_03)

const waterPatchFull_01_13 = new Entity()
waterPatchFull_01_13.setParent(scene)
waterPatchFull_01_13.addComponentOrReplace(gltfShape_8)
const transform_380 = new Transform({
  position: new Vector3(24, 0, 56),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_13.addComponentOrReplace(transform_380)
engine.addEntity(waterPatchFull_01_13)

const waterPatchFull_01_14 = new Entity()
waterPatchFull_01_14.setParent(scene)
waterPatchFull_01_14.addComponentOrReplace(gltfShape_8)
const transform_381 = new Transform({
  position: new Vector3(48, 0, 56),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_14.addComponentOrReplace(transform_381)
engine.addEntity(waterPatchFull_01_14)

const waterPatchFull_01_15 = new Entity()
waterPatchFull_01_15.setParent(scene)
waterPatchFull_01_15.addComponentOrReplace(gltfShape_8)
const transform_382 = new Transform({
  position: new Vector3(40, 0, 56),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_15.addComponentOrReplace(transform_382)
engine.addEntity(waterPatchFull_01_15)

const waterPatchFull_01_16 = new Entity()
waterPatchFull_01_16.setParent(scene)
waterPatchFull_01_16.addComponentOrReplace(gltfShape_8)
const transform_383 = new Transform({
  position: new Vector3(32, 0, 56),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_16.addComponentOrReplace(transform_383)
engine.addEntity(waterPatchFull_01_16)

const waterPatchFull_01_17 = new Entity()
waterPatchFull_01_17.setParent(scene)
waterPatchFull_01_17.addComponentOrReplace(gltfShape_8)
const transform_384 = new Transform({
  position: new Vector3(8, 0, 56),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_17.addComponentOrReplace(transform_384)
engine.addEntity(waterPatchFull_01_17)

const waterPatchFull_01_18 = new Entity()
waterPatchFull_01_18.setParent(scene)
waterPatchFull_01_18.addComponentOrReplace(gltfShape_8)
const transform_385 = new Transform({
  position: new Vector3(16.5, 0, 55.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_18.addComponentOrReplace(transform_385)
engine.addEntity(waterPatchFull_01_18)

const waterPatchFull_01_19 = new Entity()
waterPatchFull_01_19.setParent(scene)
waterPatchFull_01_19.addComponentOrReplace(gltfShape_8)
const transform_386 = new Transform({
  position: new Vector3(8.5, 0, 56),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_19.addComponentOrReplace(transform_386)
engine.addEntity(waterPatchFull_01_19)

const waterPatchFull_01_20 = new Entity()
waterPatchFull_01_20.setParent(scene)
waterPatchFull_01_20.addComponentOrReplace(gltfShape_8)
const transform_387 = new Transform({
  position: new Vector3(40, 0, 80),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_20.addComponentOrReplace(transform_387)
engine.addEntity(waterPatchFull_01_20)

const waterPatchFull_01_21 = new Entity()
waterPatchFull_01_21.setParent(scene)
waterPatchFull_01_21.addComponentOrReplace(gltfShape_8)
const transform_388 = new Transform({
  position: new Vector3(8, 0, 64),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_21.addComponentOrReplace(transform_388)
engine.addEntity(waterPatchFull_01_21)

const waterPatchFull_01_22 = new Entity()
waterPatchFull_01_22.setParent(scene)
waterPatchFull_01_22.addComponentOrReplace(gltfShape_8)
const transform_389 = new Transform({
  position: new Vector3(8, 0, 72),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_22.addComponentOrReplace(transform_389)
engine.addEntity(waterPatchFull_01_22)

const waterPatchFull_01_23 = new Entity()
waterPatchFull_01_23.setParent(scene)
waterPatchFull_01_23.addComponentOrReplace(gltfShape_8)
const transform_390 = new Transform({
  position: new Vector3(8, 0, 80),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_23.addComponentOrReplace(transform_390)
engine.addEntity(waterPatchFull_01_23)

const waterPatchFull_01_24 = new Entity()
waterPatchFull_01_24.setParent(scene)
waterPatchFull_01_24.addComponentOrReplace(gltfShape_8)
const transform_391 = new Transform({
  position: new Vector3(56, 0, 48),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_24.addComponentOrReplace(transform_391)
engine.addEntity(waterPatchFull_01_24)

const waterPatchFull_01_25 = new Entity()
waterPatchFull_01_25.setParent(scene)
waterPatchFull_01_25.addComponentOrReplace(gltfShape_8)
const transform_392 = new Transform({
  position: new Vector3(64, 0, 40),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_25.addComponentOrReplace(transform_392)
engine.addEntity(waterPatchFull_01_25)

const waterPatchFull_01_26 = new Entity()
waterPatchFull_01_26.setParent(scene)
waterPatchFull_01_26.addComponentOrReplace(gltfShape_8)
const transform_393 = new Transform({
  position: new Vector3(56, 0, 40),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_26.addComponentOrReplace(transform_393)
engine.addEntity(waterPatchFull_01_26)

const waterPatchFull_01_27 = new Entity()
waterPatchFull_01_27.setParent(scene)
waterPatchFull_01_27.addComponentOrReplace(gltfShape_8)
const transform_394 = new Transform({
  position: new Vector3(16, 0, 72),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_27.addComponentOrReplace(transform_394)
engine.addEntity(waterPatchFull_01_27)

const waterPatchFull_01_28 = new Entity()
waterPatchFull_01_28.setParent(scene)
waterPatchFull_01_28.addComponentOrReplace(gltfShape_8)
const transform_395 = new Transform({
  position: new Vector3(48, 0, 72),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_28.addComponentOrReplace(transform_395)
engine.addEntity(waterPatchFull_01_28)

const waterPatchFull_01_29 = new Entity()
waterPatchFull_01_29.setParent(scene)
waterPatchFull_01_29.addComponentOrReplace(gltfShape_8)
const transform_396 = new Transform({
  position: new Vector3(40, 0, 64),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_29.addComponentOrReplace(transform_396)
engine.addEntity(waterPatchFull_01_29)

const waterPatchFull_01_30 = new Entity()
waterPatchFull_01_30.setParent(scene)
waterPatchFull_01_30.addComponentOrReplace(gltfShape_8)
const transform_397 = new Transform({
  position: new Vector3(56, 0, 64),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_30.addComponentOrReplace(transform_397)
engine.addEntity(waterPatchFull_01_30)

const waterPatchFull_01_31 = new Entity()
waterPatchFull_01_31.setParent(scene)
waterPatchFull_01_31.addComponentOrReplace(gltfShape_8)
const transform_398 = new Transform({
  position: new Vector3(56, 0, 72),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_31.addComponentOrReplace(transform_398)
engine.addEntity(waterPatchFull_01_31)

const waterPatchFull_01_32 = new Entity()
waterPatchFull_01_32.setParent(scene)
waterPatchFull_01_32.addComponentOrReplace(gltfShape_8)
const transform_399 = new Transform({
  position: new Vector3(16, 0, 80),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_32.addComponentOrReplace(transform_399)
engine.addEntity(waterPatchFull_01_32)

const waterPatchFull_01_33 = new Entity()
waterPatchFull_01_33.setParent(scene)
waterPatchFull_01_33.addComponentOrReplace(gltfShape_8)
const transform_400 = new Transform({
  position: new Vector3(16, 0, 61.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_33.addComponentOrReplace(transform_400)
engine.addEntity(waterPatchFull_01_33)

const waterPatchFull_01_34 = new Entity()
waterPatchFull_01_34.setParent(scene)
waterPatchFull_01_34.addComponentOrReplace(gltfShape_8)
const transform_401 = new Transform({
  position: new Vector3(48, 0, 64),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_34.addComponentOrReplace(transform_401)
engine.addEntity(waterPatchFull_01_34)

const waterPatchFull_01_35 = new Entity()
waterPatchFull_01_35.setParent(scene)
waterPatchFull_01_35.addComponentOrReplace(gltfShape_8)
const transform_402 = new Transform({
  position: new Vector3(16, 0, 61.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_35.addComponentOrReplace(transform_402)
engine.addEntity(waterPatchFull_01_35)

const waterPatchFull_01_36 = new Entity()
waterPatchFull_01_36.setParent(scene)
waterPatchFull_01_36.addComponentOrReplace(gltfShape_8)
const transform_403 = new Transform({
  position: new Vector3(16, 0, 64.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_36.addComponentOrReplace(transform_403)
engine.addEntity(waterPatchFull_01_36)

const waterPatchFull_01_37 = new Entity()
waterPatchFull_01_37.setParent(scene)
waterPatchFull_01_37.addComponentOrReplace(gltfShape_8)
const transform_404 = new Transform({
  position: new Vector3(24, 0, 64),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_37.addComponentOrReplace(transform_404)
engine.addEntity(waterPatchFull_01_37)

const waterPatchFull_01_38 = new Entity()
waterPatchFull_01_38.setParent(scene)
waterPatchFull_01_38.addComponentOrReplace(gltfShape_8)
const transform_405 = new Transform({
  position: new Vector3(24, 0, 72),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_38.addComponentOrReplace(transform_405)
engine.addEntity(waterPatchFull_01_38)

const waterPatchFull_01_39 = new Entity()
waterPatchFull_01_39.setParent(scene)
waterPatchFull_01_39.addComponentOrReplace(gltfShape_8)
const transform_406 = new Transform({
  position: new Vector3(24, 0, 80),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_39.addComponentOrReplace(transform_406)
engine.addEntity(waterPatchFull_01_39)

const waterPatchFull_01_40 = new Entity()
waterPatchFull_01_40.setParent(scene)
waterPatchFull_01_40.addComponentOrReplace(gltfShape_8)
const transform_407 = new Transform({
  position: new Vector3(56, 0, 56),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_40.addComponentOrReplace(transform_407)
engine.addEntity(waterPatchFull_01_40)

const waterPatchFull_01_41 = new Entity()
waterPatchFull_01_41.setParent(scene)
waterPatchFull_01_41.addComponentOrReplace(gltfShape_8)
const transform_408 = new Transform({
  position: new Vector3(56, 0, 80),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_41.addComponentOrReplace(transform_408)
engine.addEntity(waterPatchFull_01_41)

const waterPatchFull_01_42 = new Entity()
waterPatchFull_01_42.setParent(scene)
waterPatchFull_01_42.addComponentOrReplace(gltfShape_8)
const transform_409 = new Transform({
  position: new Vector3(48, 0, 80),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_42.addComponentOrReplace(transform_409)
engine.addEntity(waterPatchFull_01_42)

const waterPatchFull_01_43 = new Entity()
waterPatchFull_01_43.setParent(scene)
waterPatchFull_01_43.addComponentOrReplace(gltfShape_8)
const transform_410 = new Transform({
  position: new Vector3(40, 0, 72),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_43.addComponentOrReplace(transform_410)
engine.addEntity(waterPatchFull_01_43)

const waterPatchFull_01_44 = new Entity()
waterPatchFull_01_44.setParent(scene)
waterPatchFull_01_44.addComponentOrReplace(gltfShape_8)
const transform_411 = new Transform({
  position: new Vector3(32, 0, 80),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_44.addComponentOrReplace(transform_411)
engine.addEntity(waterPatchFull_01_44)

const waterPatchFull_01_45 = new Entity()
waterPatchFull_01_45.setParent(scene)
waterPatchFull_01_45.addComponentOrReplace(gltfShape_8)
const transform_412 = new Transform({
  position: new Vector3(32, 0, 72),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_45.addComponentOrReplace(transform_412)
engine.addEntity(waterPatchFull_01_45)

const waterPatchFull_01_46 = new Entity()
waterPatchFull_01_46.setParent(scene)
waterPatchFull_01_46.addComponentOrReplace(gltfShape_8)
const transform_413 = new Transform({
  position: new Vector3(32, 0, 64),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_46.addComponentOrReplace(transform_413)
engine.addEntity(waterPatchFull_01_46)

const waterPatchFull_01_47 = new Entity()
waterPatchFull_01_47.setParent(scene)
waterPatchFull_01_47.addComponentOrReplace(gltfShape_8)
const transform_414 = new Transform({
  position: new Vector3(31, 0, 61.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_47.addComponentOrReplace(transform_414)
engine.addEntity(waterPatchFull_01_47)

const waterPatchFull_01_48 = new Entity()
waterPatchFull_01_48.setParent(scene)
waterPatchFull_01_48.addComponentOrReplace(gltfShape_8)
const transform_415 = new Transform({
  position: new Vector3(64, 0, 56),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_48.addComponentOrReplace(transform_415)
engine.addEntity(waterPatchFull_01_48)

const floorBaseGrass_02 = new Entity()
floorBaseGrass_02.setParent(scene)
const gltfShape_26 = new GLTFShape('models/FloorBaseGrass_02/FloorBaseGrass_02.glb')
floorBaseGrass_02.addComponentOrReplace(gltfShape_26)
const transform_416 = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_02.addComponentOrReplace(transform_416)
engine.addEntity(floorBaseGrass_02)

const floorBaseGrass_02_2 = new Entity()
floorBaseGrass_02_2.setParent(scene)
floorBaseGrass_02_2.addComponentOrReplace(gltfShape_26)
const transform_417 = new Transform({
  position: new Vector3(24, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_02_2.addComponentOrReplace(transform_417)
engine.addEntity(floorBaseGrass_02_2)

const floorBaseGrass_02_3 = new Entity()
floorBaseGrass_02_3.setParent(scene)
floorBaseGrass_02_3.addComponentOrReplace(gltfShape_26)
const transform_418 = new Transform({
  position: new Vector3(40, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_02_3.addComponentOrReplace(transform_418)
engine.addEntity(floorBaseGrass_02_3)

const floorBaseGrass_02_4 = new Entity()
floorBaseGrass_02_4.setParent(scene)
floorBaseGrass_02_4.addComponentOrReplace(gltfShape_26)
const transform_419 = new Transform({
  position: new Vector3(56, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_02_4.addComponentOrReplace(transform_419)
engine.addEntity(floorBaseGrass_02_4)

const floorBaseGrass_02_5 = new Entity()
floorBaseGrass_02_5.setParent(scene)
floorBaseGrass_02_5.addComponentOrReplace(gltfShape_26)
const transform_420 = new Transform({
  position: new Vector3(8, 0, 24),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_02_5.addComponentOrReplace(transform_420)
engine.addEntity(floorBaseGrass_02_5)

const floorBaseGrass_02_6 = new Entity()
floorBaseGrass_02_6.setParent(scene)
floorBaseGrass_02_6.addComponentOrReplace(gltfShape_26)
const transform_421 = new Transform({
  position: new Vector3(24, 0, 24),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_02_6.addComponentOrReplace(transform_421)
engine.addEntity(floorBaseGrass_02_6)

const floorBaseGrass_02_7 = new Entity()
floorBaseGrass_02_7.setParent(scene)
floorBaseGrass_02_7.addComponentOrReplace(gltfShape_26)
const transform_422 = new Transform({
  position: new Vector3(40, 0, 24),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_02_7.addComponentOrReplace(transform_422)
engine.addEntity(floorBaseGrass_02_7)

const floorBaseGrass_02_8 = new Entity()
floorBaseGrass_02_8.setParent(scene)
floorBaseGrass_02_8.addComponentOrReplace(gltfShape_26)
const transform_423 = new Transform({
  position: new Vector3(56, 0, 24),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_02_8.addComponentOrReplace(transform_423)
engine.addEntity(floorBaseGrass_02_8)

const floorBaseGrass_02_9 = new Entity()
floorBaseGrass_02_9.setParent(scene)
floorBaseGrass_02_9.addComponentOrReplace(gltfShape_26)
const transform_424 = new Transform({
  position: new Vector3(8, 0, 40),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_02_9.addComponentOrReplace(transform_424)
engine.addEntity(floorBaseGrass_02_9)

const floorBaseGrass_02_10 = new Entity()
floorBaseGrass_02_10.setParent(scene)
floorBaseGrass_02_10.addComponentOrReplace(gltfShape_26)
const transform_425 = new Transform({
  position: new Vector3(24, 0, 40),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_02_10.addComponentOrReplace(transform_425)
engine.addEntity(floorBaseGrass_02_10)

const floorBaseGrass_02_11 = new Entity()
floorBaseGrass_02_11.setParent(scene)
floorBaseGrass_02_11.addComponentOrReplace(gltfShape_26)
const transform_426 = new Transform({
  position: new Vector3(40, 0, 40),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_02_11.addComponentOrReplace(transform_426)
engine.addEntity(floorBaseGrass_02_11)

const floorBaseGrass_02_12 = new Entity()
floorBaseGrass_02_12.setParent(scene)
floorBaseGrass_02_12.addComponentOrReplace(gltfShape_26)
const transform_427 = new Transform({
  position: new Vector3(56, 0, 40),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_02_12.addComponentOrReplace(transform_427)
engine.addEntity(floorBaseGrass_02_12)

const floorBaseGrass_02_13 = new Entity()
floorBaseGrass_02_13.setParent(scene)
floorBaseGrass_02_13.addComponentOrReplace(gltfShape_26)
const transform_428 = new Transform({
  position: new Vector3(8, 0, 56),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_02_13.addComponentOrReplace(transform_428)
engine.addEntity(floorBaseGrass_02_13)

const floorBaseGrass_02_14 = new Entity()
floorBaseGrass_02_14.setParent(scene)
floorBaseGrass_02_14.addComponentOrReplace(gltfShape_26)
const transform_429 = new Transform({
  position: new Vector3(24, 0, 56),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_02_14.addComponentOrReplace(transform_429)
engine.addEntity(floorBaseGrass_02_14)

const floorBaseGrass_02_15 = new Entity()
floorBaseGrass_02_15.setParent(scene)
floorBaseGrass_02_15.addComponentOrReplace(gltfShape_26)
const transform_430 = new Transform({
  position: new Vector3(40, 0, 56),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_02_15.addComponentOrReplace(transform_430)
engine.addEntity(floorBaseGrass_02_15)

const floorBaseGrass_02_16 = new Entity()
floorBaseGrass_02_16.setParent(scene)
floorBaseGrass_02_16.addComponentOrReplace(gltfShape_26)
const transform_431 = new Transform({
  position: new Vector3(56, 0, 56),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_02_16.addComponentOrReplace(transform_431)
engine.addEntity(floorBaseGrass_02_16)

const floorBaseGrass_02_17 = new Entity()
floorBaseGrass_02_17.setParent(scene)
floorBaseGrass_02_17.addComponentOrReplace(gltfShape_26)
const transform_432 = new Transform({
  position: new Vector3(8, 0, 72),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_02_17.addComponentOrReplace(transform_432)
engine.addEntity(floorBaseGrass_02_17)

const floorBaseGrass_02_18 = new Entity()
floorBaseGrass_02_18.setParent(scene)
floorBaseGrass_02_18.addComponentOrReplace(gltfShape_26)
const transform_433 = new Transform({
  position: new Vector3(24, 0, 72),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_02_18.addComponentOrReplace(transform_433)
engine.addEntity(floorBaseGrass_02_18)

const floorBaseGrass_02_19 = new Entity()
floorBaseGrass_02_19.setParent(scene)
floorBaseGrass_02_19.addComponentOrReplace(gltfShape_26)
const transform_434 = new Transform({
  position: new Vector3(40, 0, 72),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_02_19.addComponentOrReplace(transform_434)
engine.addEntity(floorBaseGrass_02_19)

const floorBaseGrass_02_20 = new Entity()
floorBaseGrass_02_20.setParent(scene)
floorBaseGrass_02_20.addComponentOrReplace(gltfShape_26)
const transform_435 = new Transform({
  position: new Vector3(56, 0, 72),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_02_20.addComponentOrReplace(transform_435)
engine.addEntity(floorBaseGrass_02_20)

const waterPatchFull_01_49 = new Entity()
waterPatchFull_01_49.setParent(scene)
waterPatchFull_01_49.addComponentOrReplace(gltfShape_8)
const transform_436 = new Transform({
  position: new Vector3(64, 0, 80),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_49.addComponentOrReplace(transform_436)
engine.addEntity(waterPatchFull_01_49)

const waterPatchFull_01_50 = new Entity()
waterPatchFull_01_50.setParent(scene)
waterPatchFull_01_50.addComponentOrReplace(gltfShape_8)
const transform_437 = new Transform({
  position: new Vector3(64, 0, 72),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_50.addComponentOrReplace(transform_437)
engine.addEntity(waterPatchFull_01_50)

const waterPatchFull_01_51 = new Entity()
waterPatchFull_01_51.setParent(scene)
waterPatchFull_01_51.addComponentOrReplace(gltfShape_8)
const transform_438 = new Transform({
  position: new Vector3(64, 0, 64),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_51.addComponentOrReplace(transform_438)
engine.addEntity(waterPatchFull_01_51)

const waterPatchFull_01_52 = new Entity()
waterPatchFull_01_52.setParent(scene)
waterPatchFull_01_52.addComponentOrReplace(gltfShape_8)
const transform_439 = new Transform({
  position: new Vector3(64, 0, 48),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchFull_01_52.addComponentOrReplace(transform_439)
engine.addEntity(waterPatchFull_01_52)

const rockPillars_01_19 = new Entity()
rockPillars_01_19.setParent(scene)
rockPillars_01_19.addComponentOrReplace(gltfShape_21)
const transform_440 = new Transform({
  position: new Vector3(34, 10, 6),
  rotation: new Quaternion(0, 0.995184726672197, 0, -0.0980171403295605),
  scale: new Vector3(1, 1, 1)
})
rockPillars_01_19.addComponentOrReplace(transform_440)
engine.addEntity(rockPillars_01_19)

const rockPillars_01_20 = new Entity()
rockPillars_01_20.setParent(scene)
rockPillars_01_20.addComponentOrReplace(gltfShape_21)
const transform_441 = new Transform({
  position: new Vector3(46, 10, 7),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
rockPillars_01_20.addComponentOrReplace(transform_441)
engine.addEntity(rockPillars_01_20)

const rockPillars_01_21 = new Entity()
rockPillars_01_21.setParent(scene)
rockPillars_01_21.addComponentOrReplace(gltfShape_21)
const transform_442 = new Transform({
  position: new Vector3(46, 10, 22.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
rockPillars_01_21.addComponentOrReplace(transform_442)
engine.addEntity(rockPillars_01_21)

const rockPillars_01_22 = new Entity()
rockPillars_01_22.setParent(scene)
rockPillars_01_22.addComponentOrReplace(gltfShape_21)
const transform_443 = new Transform({
  position: new Vector3(34, 10, 22),
  rotation: new Quaternion(0, -0.7730104533627373, 0, 0.6343932841636459),
  scale: new Vector3(1, 1, 1)
})
rockPillars_01_22.addComponentOrReplace(transform_443)
engine.addEntity(rockPillars_01_22)

const floorBlock_04_89 = new Entity()
floorBlock_04_89.setParent(scene)
floorBlock_04_89.addComponentOrReplace(gltfShape_2)
const transform_444 = new Transform({
  position: new Vector3(47, 0, 29),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_89.addComponentOrReplace(transform_444)
engine.addEntity(floorBlock_04_89)

const floorBlock_04_90 = new Entity()
floorBlock_04_90.setParent(scene)
floorBlock_04_90.addComponentOrReplace(gltfShape_2)
const transform_445 = new Transform({
  position: new Vector3(49, 0, 29),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_90.addComponentOrReplace(transform_445)
engine.addEntity(floorBlock_04_90)

const floorBlock_04_91 = new Entity()
floorBlock_04_91.setParent(scene)
floorBlock_04_91.addComponentOrReplace(gltfShape_2)
const transform_446 = new Transform({
  position: new Vector3(51, 0, 29),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_91.addComponentOrReplace(transform_446)
engine.addEntity(floorBlock_04_91)

const floorBlock_04_92 = new Entity()
floorBlock_04_92.setParent(scene)
floorBlock_04_92.addComponentOrReplace(gltfShape_2)
const transform_447 = new Transform({
  position: new Vector3(51, 0, 27),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBlock_04_92.addComponentOrReplace(transform_447)
engine.addEntity(floorBlock_04_92)